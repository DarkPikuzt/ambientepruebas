$(document).ready(function(){

    // Declaramos variables

    var form_count = 1, previous_form, next_form, total_forms;

    //Sacamos la longitud
    total_forms = $("fieldset").length;

    console.log(total_forms)


    let muestra =7;

    console.log(muestra);


    /*Funcion para ocultar preg*/

    $(".next-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().next();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(++form_count);
    });


    $(".previous-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().prev();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(--form_count);
    });

    setProgressBarValue(form_count);
    function setProgressBarValue(value){
        var percent = parseFloat(100 / total_forms) * value;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width",percent+"%")
            .html(percent+"%");
    }


// Validamos los inputs
    $( "#register_form" ).submit(function(event) {
        
        var error_message = '';
        if(!$("#name").val()) {
            error_message+=" <strong> Faltan datos del nombre </strong>";
        }
        if(!$("#representante").val()) {
            error_message+="<br> <strong> Faltan datos del representante </strong>";
        }
        if(!$("#correoElectronico").val()) {
            error_message+="<br> <strong> Falta datos de correo </strong>";
        }


        console.log(error_message);



    // mostrar error para enviar formulario si faltal datos
        if(error_message) {
            $('.alert-danger').removeClass('hide').html(error_message);
            return false;
        } else {
            return true;
        }
    });



    /*

    let dato = $('#register_form').serialize();
    let url = "controller/insert.php";


    console.log(url);

    $.ajax({
        url: url,
        type:"POST",
        data:dato,


        success: function(data){


            console.log(dato)

            console.log(data);
        
        }
    })

    */

    



});


$('#guardar').on('click',function () {
    let addNombreUser = document.getElementById('name').value;
   

    console.log(addNombreUser);
    //validaciones;

    let datos = $('#frmAddUser').serialize();
    let url ="controllers/usuarioController.php";
    let url2 = "";


    $.ajax({
        type: "POST",
        data: datos,
        url:url ,
        success: function (data) {

            console.log(data);
            if (data == 1){
                frm = '#frmAddUser';
                limpiar(frm);
                let  urlListaUser = 'views/admin/usuario/lista.php';
                $('#listaUser').load(urlListaUser);
                alertify.success('Usuario agregado con exito');
            }else{
                console.log(data);
                alertify.error(data);

            }

        }
    });
});

