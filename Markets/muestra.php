<?php
include("conexion.php");
$selectCountry='1';

$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

// Select all the rows in the markers table
$query = mysqli_query($conn, "SELECT * FROM markers WHERE 1");

header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = mysqli_fetch_assoc($query)){
// ADD TO XML DOCUMENT NODE
$node = $dom->createElement("marker");
$newnode = $parnode->appendChild($node);

//$newnode->setAttribute("name",$row['name']);
$newnode->setAttribute("id",$row['id']);
$newnode->setAttribute("name",$row['name']);
$newnode->setAttribute("lat",$row['lat']);
$newnode->setAttribute("lng",$row['lng']);
$newnode->setAttribute("type",$row['type']);

//$newnode->setAttribute("type", utf8_encode($row['type']));
}
echo $dom->saveXML();
?>