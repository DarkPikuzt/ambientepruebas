let muestra = 5;


console.log(muestra);




/* Formulario  por paso*/ 


$(document).ready(function(){

    // Declaramos variables

    var form_count = 1, previous_form, next_form, total_forms;

    //Sacamos la longitud
    total_forms = $("fieldset").length;

    console.log(total_forms)


    let muestra =7;

    console.log(muestra);


    /*Funcion para ocultar preg*/

    $(".next-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().next();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(++form_count);
    });


    $(".previous-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().prev();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(--form_count);
    });

    setProgressBarValue(form_count);
    function setProgressBarValue(value){
        var percent = parseFloat(100 / total_forms) * value;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width",percent+"%")
            .html(percent+"%");
    }


// Validamos los inputs
    $( "#register_form" ).submit(function(event) {
        var error_message = '';
        if(!$("#email").val()) {
            error_message+="Faltan datos del Email";
        }
        if(!$("#password").val()) {
            error_message+="<br>Faltan datos de contraseña";
        }
        if(!$("#mobile").val()) {
            error_message+="<br>Faltan datos del telefono movil";
        }





// mostrar error para enviar formulario si faltal datos
        if(error_message) {
            $('.alert-success').removeClass('hide').html(error_message);
            return false;
        } else {
            return true;
        }
    });
});



/*Agregamos js para el mapa*/

function initialize() {
    // Creamos el objeto
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 12,
        center: new google.maps.LatLng(21.8260013, -98.9439123),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // creates a draggable marker to the given coords
    var vMarker = new google.maps.Marker({
        position: new google.maps.LatLng(19.4326077, -99.13320799999997),
        draggable: true
    });

    // adds a listener to the marker
    // gets the coords when drag event ends
    // then updates the input with the new coords
    google.maps.event.addListener(vMarker, 'dragend', function (evt) {
        $("#txtLat").val(evt.latLng.lat().toFixed(6));
        $("#txtLng").val(evt.latLng.lng().toFixed(6));

        map.panTo(evt.latLng);
    });

    // centers the map on markers coords
    map.setCenter(vMarker.position);

    // adds the marker on the map
    vMarker.setMap(map);
}