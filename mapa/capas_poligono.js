$(document).ready(function() {
   $("#legend-container").hide();
   var sequia_cierre2019;
   var CentrosMunicicpal;
});

/**functiones globales para totas las capas de poligono para el mousover y mouseout*/
function mouse(global){

    global.addListener('mouseover', function(event) {
        global.revertStyle();
        global.overrideStyle(event.feature, {strokeWeight: 2,strokeColor: '#ffffff'});
    });

    global.addListener('mouseout', function(event) {
        global.revertStyle();
    });

   
}
/**function que permite show and hide la capa de sequias*/
function checkbox_sequia2019(){
    $("#sequia_2019").change(function() {
        if (this.checked) {
            //alert("eee");
            /*map.data.setStyle({visible:false});*/
            pintar_poligono_sequias_2019();
            show_poput_poligono();
            $("#legend-container").show();
           
        } else {
            //alert("jajjaja");
            //map.data.setStyle({visible:false});
            if(info_window){
                info_window.setMap(null);
                info_window = null;
            }
            apagar_poligono_sequia2019();

            $("#legend-container").hide();
            /*map.data.setStyle({visible:true});
            pintar_poligono_sequias_2019(map);
            show_poput_poligono(map);*/  
        }
    });
}

/**sequias 2019 */
/**1.. se carga el json de los municipios*/
/**2.. script que pinta el estado dependiendo el tipo de sequia*/
function pintar_poligono_sequias_2019(){
    sequia_cierre2019 = new google.maps.Data({map: map});
    sequia_cierre2019.loadGeoJson('veracruz/sequias2019/61_MUN_CORRECTA.json');
    var global = sequia_cierre2019;
    mouse(global);
    // Load GeoJSON.
    //var promise = $.getJSON('veracruz/urbanas.json'); //same as map.data.loadGeoJson();
    /*var promise = $.getJSON('veracruz/sequias2019/61_MUN_CORRECTA.json');//same as map.data.loadGeoJson();
    promise.then(function(data){
        cachedGeoJson = data; //save the geojson in case we want to update its values
        map.data.addGeoJson(cachedGeoJson,{idPropertyName:"id"});  
    });*/

    sequia_cierre2019.setStyle (function (feature) {
        var color = feature.getProperty("Clave");
        console.log(color);
        if(color == "D0"){
            return ({
                fillOpacity: 0.9,
                strokeColor: 'white',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(255, 255, 0)'
        });
        }else if(color == "D1"){
            return ({
                fillOpacity: 0.9,
                strokeColor: 'white',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(255, 211, 127)'
            });
        }else if(color == "D2"){
            return ({
                fillOpacity: 0.9,
                strokeColor: 'white',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(255, 192, 0)'
            });
        }else if(color == "D3"){
            return ({
                fillOpacity: 0.9,
                strokeColor: 'white',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(255, 0, 0)'
                //visible:false
            });
        }else if(color == "D4"){
            return ({
                fillOpacity: 0.9,
                strokeColor: 'white',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(115, 0, 0)'
            });
        }
    
    });

}

function apagar_poligono_sequia2019(){
    sequia_cierre2019.forEach(function(feature) {
        sequia_cierre2019.remove(feature);
    });
}
/**sequias 2019*/
////**function que pinta el poput de informacion dependiendo el poligono que se pinte*/
function show_poput_poligono(){
    sequia_cierre2019.addListener('click', function(event) {
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }
        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers"> Municipio<br> '+
                                    event.feature.getProperty("Municipio")+
                                    '</div>'+
                                    '<table id="style_table" class="table table-sm table-bordered">'+
                                    '<thead>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '<tr>'+
                                        '<th>Tipo de sequía</th>'+'<td>'+ event.feature.getProperty("Tipo de Sequía") + '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<th>Clave</th>'+'<td>'+ event.feature.getProperty("Clave") + '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<th>Superficie (ha)</th>'+'<td>'+ event.feature.getProperty("Superficie (ha)") + '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<th>Fuente</th>'+'<td>'+ event.feature.getProperty("Fuente") + '</td>'+
                                    '</tr>'+
                                    '</tbody>'+
                                    '</table>',
            position: event.latLng, map: map
            //maxWidth: 500
        });
        //show an infowindow on click   
        /*infoWindow.setContent('<div id="style_markers"> Municipio<br> '+
                                    event.feature.getProperty("Municipio")+
                                    '</div>'+
                                    '<table id="style_table" class="table table-sm table-bordered">'+
                                    '<thead>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '<tr>'+
                                        '<th>Tipo de sequía</th>'+'<td>'+ event.feature.getProperty("Tipo de Sequía") + '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<th>Clave</th>'+'<td>'+ event.feature.getProperty("Clave") + '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<th>Superficie (ha)</th>'+'<td>'+ event.feature.getProperty("Superficie (ha)") + '</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<th>Fuente</th>'+'<td>'+ event.feature.getProperty("Fuente") + '</td>'+
                                    '</tr>'+
                                    '</tbody>'+
                                    '</table>'
        );
        var anchor = new google.maps.MVCObject();
        anchor.set("position",event.latLng);
        //infowindow.setPosition(event.latLng);
        infoWindow.open(map,anchor);
        //map.setZoom(10);
        //map.setCenter(map,event.latLng);
        //map.setCenter(this.getPosition());*/
    });

}
//////////////////////////////////
/**se agrega el panel de la leyenda*/
function agregar_panel_leyenda_sequias2019(){
    var $legendContainer = $('#legend-container'),
        $legend = $('<div id="legend">').appendTo($legendContainer);
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push($legendContainer[0]);
}

/**capa de centros de sacrificio municipal*/
/**function que permite show and hide la capa de Centros de Sacrificio*/
function checkbox_centros_sacrificio_municipal(){
    $("#SacrificioMunicipal").change(function() {
        if (this.checked) {
            //alert("isisisi");
            pintar_centros_sacrificio_municipal();
            /*pintar_poligono_sequias_2019();
            show_poput_poligono();
            $("#legend-container").show();*/
           
        } else {
            //alert("jajjaja");
            //apagar_poligono_sequia2019();
            apagar_puntos_centros_sacrificio_municicpal();
            
        }
    });
}


function pintar_centros_sacrificio_municipal(){
    CentrosMunicicpal = new google.maps.Data({map: map});
    CentrosMunicicpal.loadGeoJson('veracruz/centros_sacrificio/centros.json');
}

function apagar_puntos_centros_sacrificio_municicpal(){
    CentrosMunicicpal.forEach(function(feature) {
        CentrosMunicicpal.remove(feature);
    });
}

function show_infowindow_centros_municipal(){
    CentrosMunicicpal.addListener('click', function(event) {
        //show an infowindow on click   
        infoWindow.setContent('<div style="line-height:1.35;overflow:hidden;white-space:nowrap;"> Feature id = '+
                                    event.feature.getId() +"<br/>Feature Value = Zone " + event.feature.getProperty("description") + "</div>");
        var anchor = new google.maps.MVCObject();
        anchor.set("position",event.latLng);
        infoWindow.open(map,anchor);
    });
}