$(document).ready(function(){
  //cargar_capa_agri_pro();
    //Capas pepe
    var data_layerap;
    var data_layerci;
    var data_layerregh;
    var data_layerregh;
    var poligonoTlaltetela;
    var poligonocacao;
    var cader;
    var ddr;
    var poligonocana;
    var poligonohule;
    var poligonomaiz;
    var poligononaranja;
    var poligonopasto_estrella;
    var poligonoplatano;
    var poligonomango;
    var poligonojitomate;
    var poligonotrigo;
    var poligonocarretera;
    //capas roger
    var ren_aguacate;
    var cafe;
    var limonpersa;
    var suelovegetacion;
    var ajo;
    var avena;
    var pimienta;
    //capas fer
    var poligonopapa;
    var poligonourbana;
    var poligonoSuelosPredominantes;
    var poligonoalfalfa;
    var poligonocebolla;
    var poligonochile;
    var poligonococotero;
    var poligonofrijol;
    var poligonohaba;
    var poligonopalma_aceite;
    var poligonovainilla;
    var poligonosorgo;
    var poligonorural;
});
//FUNCION PARA OCULTAR LA CAPA DEL ESTADO DE VERACRUZ
function ocultar_capa_veracruz(){
  //OCULTAR CAPA DE VERACRUZ
  veracruz.setMap(null);
  //OCULTAR CHECK SELECCIONADO DE LA CAPA DE VERACRUZ
  $("#ver").prop("checked", false); 
}

//FUNCION DE ENCENDER Y APAR CAPA DE AGRICULTURA PROTEGIDA
function mostrar_capa_agri_pro(){
  $("#agri_protegida").change(function() {
        if (this.checked) {
          cargar_capa_agri_pro();
          texto_agri_pro();
          ocultar_capa_veracruz();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_agri_pro();
          
        }
  });
}

function texto_agri_pro(){
   //listen for click events
     data_layerap.addListener('click', function(event) {
        if(info_window){
              info_window.setMap(null);
              info_window = null;
        }
        var imagen = event.feature.getProperty("Imagen");
        info_window = new google.maps.InfoWindow({
          content: '<div id="style_markers"> Municipio<br> '+
                      event.feature.getProperty("Name")+ 
                      '</div>'+
                      '<table id="style_table" class="table table-sm table-bordered">'+
                      '<thead>'+
                      '</thead>'+
                      '<tbody>'+
                      '<tr>'+
                      '<th>Cultivo</th>'+'<td>'+ event.feature.getProperty("Cultivo") + '</td>'+
                      '</tr>'+
                      '<tr>'+
                      '<th>Tipo de Instalación</th>'+'<td>'+ event.feature.getProperty("Tipo de instalación") + '</td>'+
                      '</tr>'+
                      '<tr>'+
                      '<th>Superficie (m2)</th>'+'<td>'+ event.feature.getProperty("Superficie (m2)") + '</td>'+
                      '</tr>'+
                      '<tr>'+
                      '<th>Estatus</th>'+'<td>'+ event.feature.getProperty("Estatus") + '</td>'+
                      '</tr>'+
                      '<tr>'+
                      '<th>Fuente</th>'+'<td>'+ event.feature.getProperty("Fuente") + '</td>'+
                      '</tr>'+
                      '<tr>'+
                      '<th>Imagen</th>'+'<td><img src="'+imagen+'" style="height:8%"></td>'+
                      '</tr>'+
                      '</tbody>'+
                      '</table>',
          position: event.latLng, map: map
          //maxWidth: 500
        });
      });
}

function cargar_capa_agri_pro(){
  data_layerap = new google.maps.Data({map: map});
  data_layerap.loadGeoJson('veracruz/agricultura_protegida/agricultura_protegida.json');
  var global = data_layerap;
  mouse(global);
  data_layerap.setStyle (function (feature) {
      return ({
        fillOpacity: 0.2,
        strokeColor: '#27D83C',
        strokeOpacity: 0.9,
        strokeWeight: 3,
        fillColor: '#97FB9E'
      });
  }); 
}

function apagar_agri_pro(){
  data_layerap.forEach(function(feature) {
    data_layerap.remove(feature);
  });   
}

//FUNCION DE ENCENDER Y APAGAR CAPA DE COBERTURA DE INGENIO
function mostrar_capa_cobertura_ing(){
  $("#cobertura_ing").change(function() {
        if (this.checked) {
          cargar_capa_cobertura_ing();
          texto_cobertura_ingenio();
          ocultar_capa_veracruz();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_capa_cobertura_ing();
        }
  });
}

function cargar_capa_cobertura_ing(){
  // Load GeoJSON.
  data_layerci = new google.maps.Data({map: map});
  data_layerci.loadGeoJson('veracruz/cobertura_ingenio/cobertura_ingenio.json');
  var global = data_layerci;
  mouse(global);

  data_layerci.setStyle (function (feature) {
     
      return ({
        fillOpacity: 0.2,
        strokeColor: '#8E7902',
        strokeOpacity: 0.9,
        strokeWeight: 3,
        fillColor: '#FBF9D0'
      });
  });
}
function texto_cobertura_ingenio(){
   //listen for click events
     data_layerci.addListener('click', function(event) {
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }
        var contenido = event.feature.getProperty("description")
        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers"> Ingenio<br> '+
                      event.feature.getProperty("Name")+ 
                      '</div><br>'+ contenido+ 
                      '</div>',
            position: event.latLng, map: map
            //maxWidth: 500
        });
      });
}

//APAGAR POLIGONO DE COBERTURA DE INGENIOS
function apagar_capa_cobertura_ing(){
   data_layerci.forEach(function(feature) {
    data_layerci.remove(feature);
  });
}

//FUNCIÓN DE ENCENDER Y APAGAR CAPA DE REGIONES HIDROLOGICAS
function mostrar_capa_reg_hidro(){
  $("#reg_hidro").change(function() {
        if (this.checked) {
          cargar_capa_reg_hidro();
          texto_reg_hidro();
          ocultar_capa_veracruz();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_capa_reg_hidro();
        }
  });
}

function cargar_capa_reg_hidro(){
  // Load GeoJSON.
  data_layerregh = new google.maps.Data({map: map});
  data_layerregh.loadGeoJson('veracruz/regiones_hidrologicas/regiones_hidrologicas.json');
  var global = data_layerregh;
  mouse(global);

  data_layerregh.setStyle (function (feature) {
      var color = feature.getProperty("Name");
      console.log(color);

      if(color == "Balsas"){
            return ({
                fillOpacity: 0.5,
                strokeColor: '#6e6e6e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#3098b0'
        });
        }else if(color == "Pánuco"){
            return ({
                fillOpacity: 0.5,
                strokeColor: '#6e6e6e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#8dc1f2'
            });
        }else if(color == "Norte de Veracruz"){
          return ({
                fillOpacity: 0.5,
                strokeColor: '#6e6e6e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b1d6de'
            });
        }else if(color == "Papaloapan"){
          return ({
                fillOpacity: 0.5,
                strokeColor: '#6e6e6e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#7996b0'
            });
        }else if(color == "Coatzacoalcos"){
          return ({
                fillOpacity: 0.5,
                strokeColor: '#6e6e6e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#3e9ede'
            });
        }
  });
}

function texto_reg_hidro(){
   //listen for click events
     data_layerregh.addListener('click', function(event) {
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }
        var contenido = event.feature.getProperty("description")
        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers"> Región<br> '+
                      event.feature.getProperty("Name")+ 
                      '</div><br>'+ event.feature.getProperty("description")+ 
                      '</div>',
            position: event.latLng, map: map
            //maxWidth: 500
        });
     });
}

function apagar_capa_reg_hidro(){
   data_layerregh.forEach(function(feature) {
    data_layerregh.remove(feature);
  });
}

//FUNCIÓN DE ENCENDER Y APAGAR CAPA DE DISTRITOS DE RIEGO
function mostrar_capa_dis_riego(){
  $("#dis_riego").change(function() {
        if (this.checked) {
          cargar_capa_dis_riego();
          texto_dis_riego();
          ocultar_capa_veracruz();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_capa_dis_riego();
        }
  });
}

function cargar_capa_dis_riego(){
  // Load GeoJSON.
  data_layerdisrig = new google.maps.Data({map: map});
  data_layerdisrig.loadGeoJson('veracruz/distritos_riego/distritos_riego.json');
  var global = data_layerdisrig;
  mouse(global);

  data_layerdisrig.setStyle (function (feature) {
      var color = feature.getProperty("Name");
      console.log(color);

        return ({
            fillOpacity: 0.2,
            strokeColor: '#a3ff73',
            strokeOpacity: 0.9,
            strokeWeight: 2,
            fillColor: '#38a800'
        }); 
  });
}

function texto_dis_riego(){
   //listen for click events
     data_layerdisrig.addListener('click', function(event) {
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }
        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers">'+
                      event.feature.getProperty("Name")+ 
                      '</div><br>'+ event.feature.getProperty("description")+ 
                      '</div>',
            position: event.latLng, map: map
            //maxWidth: 500
        });
      });
}

function apagar_capa_dis_riego(){
   data_layerdisrig.forEach(function(feature) {
    data_layerdisrig.remove(feature);
  });
}

///////CAPA DE CITRICOS

/**POLIGONO DE TLALTETELA*/
function pintar_poligono_citricos_tlaltetela(){
    poligonoTlaltetela = new google.maps.Data({map: map});
    poligonoTlaltetela.loadGeoJson('veracruz/citrico_tlaltetela/tlaltetela.json'); 
    var global = poligonoTlaltetela;
    mouse(global);

    poligonoTlaltetela.setStyle (function (feature) {
        var color = feature.getProperty("altitudeMode");
        console.log(color);
        if(color == "clampToGround"){
            return ({
                fillOpacity: 0.1,
                strokeColor: '#05b616',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#a3f0aa'
            });
        }
    });

}

function apagar_citricos_tlaltetela(){
    poligonoTlaltetela.forEach(function(feature) {
        poligonoTlaltetela.remove(feature);
    });
}

/**function de el infowindow*/
function texto_citrico_tlaltetela(){
    //listen for click events
    poligonoTlaltetela.addListener('click', function(event) {
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Municipio<br> '+
                       event.feature.getProperty("Name")+ 
                       '</div><br>'+ event.feature.getProperty("description")+ 
                       '<div>Fuente: LABORATORIO (2019)</div>',
             position: event.latLng, map: map
             //maxWidth: 500
         });

         /*infoWindow.setContent('<div id="style_markers"> Municipio<br> '+
                       event.feature.getProperty("Name")+ 
                       '</div><br>'+ event.feature.getProperty("description")+ 
                       '<div>Fuente: LABORATORIO (2019)</div>'*//*+
                       '<table id="style_table" class="table table-sm table-bordered">'+
                       '<thead>'+
                       '</thead>'+
                       '<tbody>'+
                       '<tr>'+
                       '<th>Municipio</th>'+'<td>'+ event.feature.getProperty("description") + '</td>'+
                       '</tr>'+
                       '<tr>'+
                       '<th>Superficie (Ha)</th>'+'<td>'+ event.feature.getProperty("Superficie (Ha)") + '</td>'+
                       '</tr>'+
                       '<tr>'+
                       '<th>Fuente</th>'+'<td>CONADESUCA 2015</td>'+
                       '</tr>'+
                       '</tbody>'+
                       '</table>'
                       );
         var anchor = new google.maps.MVCObject();
         anchor.set("position",event.latLng);
         infoWindow.open(map,anchor);
         map.setZoom(18);
         map.setCenter(anchor.getPosition());*/  
       });
}


/**capa de citricos de tlaltetela/
/**function que permite show and hide la capa de Citricos de tlaltetela*/
function checkbox_citricos_tlaltetela(){
    $("#citrico_tlal").change(function() {
        if (this.checked) {
          pintar_poligono_citricos_tlaltetela();
          ocultar_capa_veracruz();
          texto_citrico_tlaltetela();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_citricos_tlaltetela();     
        }
    });
}

/**FUNCTIONES PARA LA CAPA DE POLIGONOS DE LIMITES CADER 2019*/
function pintar_poligono_limites_cader(){
    cader = new google.maps.Data({map: map});
    cader.loadGeoJson('veracruz/Limites_CADER/cader.json');
    var global = cader;
    mouse(global);
    cader.setStyle (function (feature) {
        var color = feature.getProperty("altitudeMode");
        console.log(color);
        if(color == "clampToGround"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#F7720B',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(138,221,45,0.1)'
            });
        }
    
    });
   
}
/**functiones globales para totas las capas de poligono para el mousover y mouseout*/
function mouse(global){

    global.addListener('mouseover', function(event) {
        global.revertStyle();
        global.overrideStyle(event.feature, {strokeWeight: 2,strokeColor: '#ffffff'});
    });

    global.addListener('mouseout', function(event) {
        global.revertStyle();
    });

   
}
/**function para apagar la capa de limites cader*/
function apagar_limites_cader(){
    cader.forEach(function(feature) {
        cader.remove(feature);
    });
}

/**function de el infowindow de limites cader*/
function texto_limites_cader(){
    //listen for click
    //.log(cader);
    cader.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }
        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers"> Municipio<br> '+e.feature.getProperty("Name")+'</div><br>'+contenido,
            position: e.latLng, map: map
        });
    });    
}


/**function que permite show and hide la capa de limites cader*/
function checkbox_limites_cader(){
    $("#limites_cader").change(function() {
        if (this.checked) {
           //alert("JAJAJAJ");
           pintar_poligono_limites_cader();
           texto_limites_cader();
           ocultar_capa_veracruz();

        } else {
           //alert("ONONONO");
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
           apagar_limites_cader(); 
        }
    });
}

////////////////////////////////////////////////
/**function que pinta el poligono de limites DDR*/
function pintar_poligono_limites_ddr(){
    ddr = new google.maps.Data({map: map});
    ddr.loadGeoJson('veracruz/Limites_ddr/ddr.json');
    var global = ddr;
    mouse(global);
    ddr.setStyle (function (feature) {
        var color = feature.getProperty("altitudeMode");
        console.log(color);
        if(color == "clampToGround"){
            return ({
                fillOpacity: 0.3,
                strokeColor: 'rgba(0,255,0,0.6)',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: 'rgba(138,221,45,0)'
            });
        }
    
    });

}

/**function para apagar la capa de limites ddr*/
function apagar_limites_ddr(){
    ddr.forEach(function(feature) {
        ddr.remove(feature);
    });
}


/**function de el infowindow de limites cader*/
function texto_limites_ddr(){
    //listen for click
    //.log(cader);
    ddr.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }
        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers"> Municipio<br> '+e.feature.getProperty("Name")+'</div><br>'+contenido,
            position: e.latLng, map: map
        });
    });    
}


/**function que permite show and hide la capa de limites cader*/
function checkbox_limites_ddr(){
    $("#limites_ddr").change(function() {
        if (this.checked) {
           //alert("JAJAJAJ");
           pintar_poligono_limites_ddr();
           texto_limites_ddr();
           ocultar_capa_veracruz();
          
        } else {
           //alert("ONONONO");
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_limites_ddr();
        }
    });
}

/**POLIGONO DE POTENCIAL CACAO*/
function pintar_poligono_potencial_cacao(){
    poligonocacao = new google.maps.Data({map: map});
    poligonocacao.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_cacao.json'); 
    var global = poligonocacao;
    mouse(global);
    poligonocacao.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        //console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#e67737',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e67737'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#cfb08a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#cfb08a'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#f5b042',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5b042'
            });
        }
    });

}

function apagar_poligono_potencial_cacao(){
    poligonocacao.forEach(function(feature) {
        poligonocacao.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_cacao(){
    //listen for click events
    poligonocacao.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
         /*var imagen = event.feature.getProperty("Feature");
         console.log(imagen);
         infoWindow.setContent('<div id="style_markers"> Potencial Productivo<br> '+ 
                       '</div><br>'+ event.feature.getProperty("description") 
                       );
         var anchor = new google.maps.MVCObject();
         anchor.set("position",event.latLng);
         infoWindow.open(map,anchor);*/
         //map.setZoom(18);
         //map.setCenter(anchor.getPosition());  
       });
}


/**capa de cacao*/
function checkbox_poten_cacao(){
    $("#poten_cacao").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_cacao();
          ocultar_capa_veracruz();
          texto_poligono_potencial_cacao();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_cacao();     
        }
    });
}

/**POLIGONO DE POTENCIAL CAÑA*/
function pintar_poligono_potencial_cana(){
    poligonocana = new google.maps.Data({map: map});
    poligonocana.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_cana.json'); 
    var global = poligonocana;
    mouse(global);
    poligonocana.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        //console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#5F3003',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#5F3003'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#B76F2A',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#B76F2A'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FFD0A2',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FFD0A2'
            });
        }
    });

}

function apagar_poligono_potencial_cana(){
    poligonocana.forEach(function(feature) {
        poligonocana.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_cana(){
    //listen for click events
    poligonocana.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de caña*/
function checkbox_poten_cana(){
    $("#poten_cana").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_cana();
          ocultar_capa_veracruz();
          texto_poligono_potencial_cana();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_cana();     
        }
    });
}

/**POLIGONO DE POTENCIAL HULE*/
function pintar_poligono_potencial_hule(){
    poligonohule = new google.maps.Data({map: map});
    poligonohule.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_hule.json'); 
    var global = poligonohule;
    mouse(global);
    poligonohule.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        //console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#22A604',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#22A604'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#828D04',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#828D04'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#E7FF95',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#E7FF95'
            });
        }
    });

}

function apagar_poligono_potencial_hule(){
    poligonohule.forEach(function(feature) {
        poligonohule.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_hule(){
    //listen for click events
    poligonohule.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de hule*/
function checkbox_poten_hule(){
    $("#poten_hule").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_hule();
          ocultar_capa_veracruz();
          texto_poligono_potencial_hule();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_hule();     
        }
    });
}

/**POLIGONO DE POTENCIAL MAIZ*/
function pintar_poligono_potencial_maiz(){
    poligonomaiz = new google.maps.Data({map: map});
    poligonomaiz.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_maiz.json'); 
    var global = poligonomaiz;
    mouse(global);
    poligonomaiz.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FECF37',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FECF37'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#71ac06',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#71ac06'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#b5db38',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b5db38'
            });
        }
    });

}

function apagar_poligono_potencial_maiz(){
    poligonomaiz.forEach(function(feature) {
        poligonomaiz.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_maiz(){
    //listen for click events
    poligonomaiz.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de maiz*/
function checkbox_poten_maiz(){
    $("#poten_maiz").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_maiz();
          ocultar_capa_veracruz();
          texto_poligono_potencial_maiz();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_maiz();     
        }
    });
}

/**POLIGONO DE POTENCIAL NARANJA*/
function pintar_poligono_potencial_naranja(){
    poligononaranja = new google.maps.Data({map: map});
    poligononaranja.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_naranja.json'); 
    var global = poligononaranja;
    mouse(global);
    poligononaranja.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#fe9003',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#fe9003'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#b5a10a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b5a10a'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#dffe97',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#dffe97'
            });
        }
    });

}

function apagar_poligono_potencial_naranja(){
    poligononaranja.forEach(function(feature) {
        poligononaranja.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_naranja(){
    //listen for click events
    poligononaranja.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de naranja*/
function checkbox_poten_naranja(){
    $("#poten_naranja").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_naranja();
          ocultar_capa_veracruz();
          texto_poligono_potencial_naranja();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_naranja();     
        }
    });
}

/**POLIGONO DE POTENCIAL PASTO ESTRELLA*/
function pintar_poligono_potencial_pasto_estrella(){
    poligonopasto_estrella = new google.maps.Data({map: map});
    poligonopasto_estrella.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_pasto_estrella.json'); 
    var global = poligonopasto_estrella;
    mouse(global);
    poligonopasto_estrella.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#fd1313',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#fd1313'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#49a507',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#49a507'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#c0fa81',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#c0fa81'
            });
        }
    });

}

function apagar_poligono_potencial_pasto_estrella(){
    poligonopasto_estrella.forEach(function(feature) {
        poligonopasto_estrella.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_pasto_estrella(){
    //listen for click events
    poligonopasto_estrella.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de pasto_estrella*/
function checkbox_poten_pasto_estrella(){
    $("#poten_pasto_estrella").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_pasto_estrella();
          ocultar_capa_veracruz();
          texto_poligono_potencial_pasto_estrella();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_pasto_estrella();     
        }
    });
}

/**POLIGONO DE POTENCIAL PLATANO*/
function pintar_poligono_potencial_platano(){
    poligonoplatano = new google.maps.Data({map: map});
    poligonoplatano.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_platano.json'); 
    var global = poligonoplatano;
    mouse(global);
    poligonoplatano.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FEF620',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FEF620'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#CA973E',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#CA973E'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#889017',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#889017'
            });
        }
    });

}

function apagar_poligono_potencial_platano(){
    poligonoplatano.forEach(function(feature) {
        poligonoplatano.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_platano(){
    //listen for click events
    poligonoplatano.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de platano*/
function checkbox_poten_platano(){
    $("#poten_platano").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_platano();
          ocultar_capa_veracruz();
          texto_poligono_potencial_platano();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_platano();     
        }
    });
}

/**POLIGONO DE POTENCIAL MANGO*/
function pintar_poligono_potencial_mango(){
    poligonomango = new google.maps.Data({map: map});
    poligonomango.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_mango.json'); 
    var global = poligonomango;
    mouse(global);
    poligonomango.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#e1361e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e1361e'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#de7b03',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de7b03'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#f5c039',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5c039'
            });
        }
    });

}

function apagar_poligono_potencial_mango(){
    poligonomango.forEach(function(feature) {
        poligonomango.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_mango(){
    //listen for click events
    poligonomango.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de mango*/
function checkbox_poten_mango(){
    $("#poten_mango").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_mango();
          ocultar_capa_veracruz();
          texto_poligono_potencial_mango();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_mango();     
        }
    });
}

/**POLIGONO DE POTENCIAL JITOMATE*/
function pintar_poligono_potencial_jitomate(){
    poligonojitomate = new google.maps.Data({map: map});
    poligonojitomate.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_jitomate.json'); 
    var global = poligonojitomate;
    mouse(global);
    poligonojitomate.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#dc0000',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#dc0000'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#fe5e48',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#fe5e48'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#533a02',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#533a02'
            });
        }
    });

}

function apagar_poligono_potencial_jitomate(){
    poligonojitomate.forEach(function(feature) {
        poligonojitomate.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_jitomate(){
    //listen for click events
    poligonojitomate.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de jitomate*/
function checkbox_poten_jitomate(){
    $("#poten_jitomate").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_jitomate();
          ocultar_capa_veracruz();
          texto_poligono_potencial_jitomate();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_jitomate();     
        }
    });
}

/**POLIGONO DE POTENCIAL TRIGO*/
function pintar_poligono_potencial_trigo(){
    poligonotrigo = new google.maps.Data({map: map});
    poligonotrigo.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_trigo.json'); 
    var global = poligonotrigo;
    mouse(global);
    poligonotrigo.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#fcd671',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#fcd671'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#ad5d04',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#ad5d04'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#b9b598',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b9b598'
            });
        }
    });

}

function apagar_poligono_potencial_trigo(){
    poligonotrigo.forEach(function(feature) {
        poligonotrigo.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_trigo(){
    //listen for click events
    poligonotrigo.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers"> Potencial Productivo<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de trigo*/
function checkbox_poten_trigo(){
    $("#poten_trigo").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_trigo();
          ocultar_capa_veracruz();
          texto_poligono_potencial_trigo();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_trigo();     
        }
    });
}

//CAPAS ROGER
/**PINTAR POLIGONO DE POTENCIAL DE AGUACATE*/
function pintar_poligono_potencial_aguacate(){
    ren_aguacate = new google.maps.Data({map: map});
    ren_aguacate.loadGeoJson('veracruz/potencial_productivo/Potencial_Aguacate.json');
    var global = ren_aguacate;
    mouse(global);
    ren_aguacate.setStyle (function (feature) {
        var color = feature.getProperty("Rendimiento");
        console.log(color);
        if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F5E794',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#F5E794'
            });
        }else if(color == "Medio"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#8B6D10',
                strokeOpacity: 0.9,
                strokeWeight: 3,
                fillColor: '#8B6D10'
            });
        }else if(color == "Alto"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#79932B',
                strokeOpacity: 0.9,
                strokeWeight: 3,
                fillColor: '#79932B'
            });
        }

    });

}

/**function de el infowindow de potencial de aguacate*/
function texto_potencial_aguacate(){
    //listen for click
    //.log(cader);
    ren_aguacate.addListener('click', function(e) {
        
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });

        info_window = new google.maps.InfoWindow({
            content: '<div id="style_markers"><center><strong>'+
                    e.feature.getProperty("Name")+'</strong></center>' +
                    '<table id="style_table" class="table table-sm table-bordered">'+
                    '<tr>'+
                        '<th>Cultivo</th>'+'<td>'+ e.feature.getProperty("Cultivo") + '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<th>Rendimiento</th>'+'<td>'+ e.feature.getProperty("Rendimiento") + '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<th>Superficie (ha)</th>'+'<td>'+ e.feature.getProperty("Superficie (ha)") + '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<th>Fuente</th>'+'<td>'+ e.feature.getProperty("Fuente") + '</td>'+
                    '</tr>'+
                    "</table>"+
            '</div>',
            position: e.latLng, map: map
        });
    });    
}

/**function para apagar la capa de potencial de aguacate*/
function apagar_potencial_aguacate(){
    ren_aguacate.forEach(function(feature) {
        ren_aguacate.remove(feature);
    });
}

/**function que permite show and hide la capa de potencial de aguacate*/
function checkbox_potencial_aguacate(){
    $("#rendimiento_aguacate").change(function() {
        if (this.checked) {
            pintar_poligono_potencial_aguacate();
            texto_potencial_aguacate();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_potencial_aguacate();
        }
    });
}

///////////////////////////////////////////////////////////////////////////
/**PINTAR POLIGONO DE POTENCIAL DE CAFE*/
function pintar_poligono_potencial_cafe(){
    cafe = new google.maps.Data({map: map});
    cafe.loadGeoJson('veracruz/potencial_productivo/cafe.json');
    var global = cafe;
    mouse(global);
    cafe.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F1D5B8',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#F1D5B8'
            });
        }else if(color == "Medio"){
            return ({
                fillOpacity: 0.2,
                strokeColor: 'red',
                strokeOpacity: 0.9,
                strokeWeight: 3,
                fillColor: 'red'
            });
        }else if(color == "Alto"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#7A1E20',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#7A1E20'
            });
        }
    });
    
}

/**function para apagar la capa de potencial de cafe*/
function apagar_potencial_cafe(){
    cafe.forEach(function(feature) {
        cafe.remove(feature);
    });
}

/**function de el infowindow del poligono de potencial de cafe*/
function texto_potencial_cafe(){
    cafe.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });
        info_window = new google.maps.InfoWindow({
            content: '<div Potencial id="style_markers">Potencial Productivo</div><br>'+contenido,
            position: e.latLng, map: map,
            
        });
    });
}

/**function que permite show and hide la capa de potencial de cafe*/
function checkbox_potencial_cafe(){
    $("#rendimiento_cafe").change(function() {
        if (this.checked) {
            pintar_poligono_potencial_cafe();
            texto_potencial_cafe();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_potencial_cafe();
        }
    });
}
///////////////////////////////////////////////////////////////////////////
/**PINTAR POLIGONO DE POTENCIAL DE LIMON PERSA */
function pintar_poligono_potencial_limon_persa(){
    limonpersa = new google.maps.Data({map: map});
    limonpersa.loadGeoJson('veracruz/potencial_productivo/limon_persa.json');
    var global = limonpersa;
    mouse(global);
    limonpersa.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F8F031',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#F8F031'
            });
        }else if(color == "Medio"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#C9E578',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#C9E578'
            });
        }else if(color == "Alto"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#9DC423',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#3E8340'
            });
        }
    });   
}

/**function para apagar la capa de potencial de cafe*/
function apagar_potencial_limon_persa(){
    limonpersa.forEach(function(feature) {
        limonpersa.remove(feature);
    });
}

/**function del infowindow del poligono limon persa*/
function texto_potencial_limon_persa(){
    limonpersa.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });
        info_window = new google.maps.InfoWindow({
            content: '<div Potencial id="style_markers">Potencial Productivo</div><br>'+contenido,
            position: e.latLng, map: map,
            
        });
    });
}
/**function que permite show and hide la capa de limon persa*/
function checkbox_potencial_limon_persa(){
    $("#limon_persa").change(function() {
        if (this.checked) {
            pintar_poligono_potencial_limon_persa();
            texto_potencial_limon_persa();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_potencial_limon_persa();
        }
    });
}
///////////////////////////////////////////////////////////////////////
/**PINTAR POLIGONO DE Suelo y Vegetacion*/
function pintar_poligono_suelo_vegetacion(){
    suelovegetacion = new google.maps.Data({map: map});
    suelovegetacion.loadGeoJson('veracruz/Suelo_Vegetacion/suelo_vegetacion.json');
    var global = suelovegetacion;
    mouse(global);
    suelovegetacion.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Agricultura de humedad") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#c3fa6b',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#c3fa6b'
            });
        }else if (color == "Agricultura de riego") {
            return ({
                fillOpacity: 0.3,
                strokeColor: '#b0c22b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b0c22b'
            });   
        }else if(color == "Agricultura de temporal"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#a7b56b',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#a7b56b'
            }); 
        }else if(color == "Área deprovista de vegetación"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#aab88d',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#aab88d'
            }); 
        }else if(color == "Bosque cultivado"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#d3eb91',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#d3eb91'
            }); 
        }else if(color == "Bosque de coníferas"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#90cf32',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#90cf32'
            });
        }else if(color == "Bosque de encino"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#c1d496',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#c1d496'
            });
        }else if(color == "Bosque mesófilo de montaña"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#a3b33b',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#a3b33b'
            });
        }else if(color == "Matorral xerófilo"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#e6f0bd',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e6f0bd'
            });
        }else if(color == "Otros tipos de vegetación"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#acdb65',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#acdb65'
            });
        }else if(color == "Pastizal"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#a9c259',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#a9c259'
            });
        }else if(color == "Pastizal cultivado"){
            return ({
                fillOpacity: 0.3,
                strokeColor: '#83b336',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#83b336'
            });
        }else if(color == "Selva caducifolia"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#c4e649',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#c4e649'
            });
        }else if(color == "Selva perennifolia"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#d7fa84',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#d7fa84'
            });
        }else if(color == "Selva subcaducifolia"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#b3fa41',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#b3fa41'
            });
        }else if(color == "Sin vegetación aparente"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#d4f736',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#d4f736'
            });
        }else if(color == "Vegetación hidrófila"){
            return ({
                fillOpacity: 0.4,
                strokeColor: '#c4cf7c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#c4cf7c'
            });
        }else if(color == "Vegetación inducida"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#c0d654',
                strokeOpacity: 0.9,
                strokeWeight: 1,
                fillColor: '#c0d654'
            });
        }
    });    
}

/**function para apagar la capa de potencial de cafe*/
function apagar_vegetacion_suelo(){
    suelovegetacion.forEach(function(feature) {
        suelovegetacion.remove(feature);
    });
}

/**function del infowindow del poligono Vegetacion y Suelo*/
function texto_vegetacion_suelo(){
    suelovegetacion.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        var nombre =  e.feature.getProperty("Name");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });
        info_window = new google.maps.InfoWindow({
            content: '<div Potencial id="style_markers">'+nombre+'</div><br>'+contenido,
            position: e.latLng, map: map,
            
        });
    });
}

/**function que permite show and hide la capa de Vegetacion y Suelo*/
function checkbox_potencial_vegetacion_suelo(){
    $("#vegetacion").change(function() {
        if (this.checked) {
            pintar_poligono_suelo_vegetacion();
            texto_vegetacion_suelo();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_vegetacion_suelo();
        }
    });
}

/////////////////////////////////////////////////////////////////
/**FUNCTION QUE PERMITE PINTAR EL POLIGONO DE POTENCIAL DE AJO*/
function pintar_poligono_potencial_ajo(){
    ajo = new google.maps.Data({map: map});
    ajo.loadGeoJson('veracruz/potencial_productivo/ajo.json');
    var global = ajo;
    mouse(global);
    ajo.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#A59B7C',
                strokeOpacity: 0.9,
                strokeWeight: 1.8,
                fillColor: '#A59B7C'
            });
        }else if(color == "Medio"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#E2A54C',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#E2A54C'
            });
        }else if(color == "Alto"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#D296A0',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#D296A0'
            });
        }
    });
    
}

/**function para apagar la capa de potencial de ajo*/
function apagar_vegetacion_ajo(){
    ajo.forEach(function(feature) {
        ajo.remove(feature);
    });
}

/**function del infowindow del poligono AJO*/
function texto_potencial_ajo(){
    ajo.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        var nombre =  e.feature.getProperty("Name");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });
        info_window = new google.maps.InfoWindow({
            content: '<div Potencial id="style_markers">Potencial Productivo</div><br>'+contenido,
            position: e.latLng, map: map,
            
        });
    });
}


/**function que permite show and hide la capa de potencial de ajo*/
function checkbox_potencial_ajo(){
    $("#ajo").change(function() {
        if (this.checked) {
            pintar_poligono_potencial_ajo();
            texto_potencial_ajo();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_vegetacion_ajo();
        }
    });
}
/////////////////////////////////////////////////////////////////////
/**FUNCTION QUE PERMITE PINTAR EL POLIGONO DE POTENCIAL DE AVENA*/
function pintar_poligono_potencial_avena(){
    avena = new google.maps.Data({map: map});
    avena.loadGeoJson('veracruz/potencial_productivo/avena.json');
    var global = avena;
    mouse(global);
    avena.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F3DAAE',
                strokeOpacity: 0.7,
                strokeWeight: 1,
                fillColor: '#F3DAAE'
            });
        }else if(color == "Medio"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F7D498',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F7D498'
            });
        }else if(color == "Alto"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#7F4734',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#7F4734'
            });
        }
    });

}

/**function para apagar la capa de potencial de avena*/
function apagar_potencial_avena(){
    avena.forEach(function(feature) {
        avena.remove(feature);
    });
}

/**function del infowindow del poligono avena*/
function texto_potencial_avena(){
    avena.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        var nombre =  e.feature.getProperty("Name");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });
        info_window = new google.maps.InfoWindow({
            content: '<div Potencial id="style_markers">Potencial Productivo</div><br>'+contenido,
            position: e.latLng, map: map,
            
        });
    });
}

/**function que permite show and hide la capa de potencial de avena*/
function checkbox_potencial_avena(){
    $("#avena").change(function() {
        if (this.checked) {
            pintar_poligono_potencial_avena();
            texto_potencial_avena();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_potencial_avena();
        }
    });
}

/////////////////////////////////////////////////////////////////////////
/**FUNCTION QUE PINTA EL POLIGONO DE POTENCIAL DE AVENA*/
function pintar_poligono_potencial_pimienta(){
    pimienta = new google.maps.Data({map: map});
    pimienta.loadGeoJson('veracruz/potencial_productivo/pimienta.json');
    pimienta.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F8ECD4',
                strokeOpacity: 0.7,
                strokeWeight: 2,
                fillColor: '#F8ECD4'
            });
        }else if(color == "Medio"){
            return ({
                fillOpacity: 0.2,
                strokeColor: '#B6A48D',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#B6A48D'
            });
        }else if(color == "Alto"){
            return ({
                fillOpacity: 0.2,
                strokeColor: 'red',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: 'red'
            });
        }
    });

}

/**function para apagar la capa de potencial de pimienta*/
function apagar_potencial_pimienta(){
    pimienta.forEach(function(feature) {
        pimienta.remove(feature);
    });
}

/**function del infowindow del poligono pimienta*/
function texto_potencial_pimienta(){
        pimienta.addListener('click', function(e) {
        var contenido =  e.feature.getProperty("description");
        var nombre =  e.feature.getProperty("Name");
        if(info_window){
            info_window.setMap(null);
            info_window = null;
        }

        setTimeout(function(){
            //map.setZoom(10);
            map.setCenter(e.latLng);
        });
        info_window = new google.maps.InfoWindow({
            content: '<div Potencial id="style_markers">Potencial Productivo</div><br>'+contenido,
            position: e.latLng, map: map,
            
        });
    });
}

/**function que permite show and hide la capa de potencial de pimienta*/
function checkbox_potencial_pimienta(){
    $("#pimienta").change(function() {
        if (this.checked) {
            pintar_poligono_potencial_pimienta();
            texto_potencial_pimienta();
            ocultar_capa_veracruz();
        } else {
           if(info_window){
            info_window.setMap(null);
            info_window = null;
            }
            apagar_potencial_pimienta();
        }
    });
}

//CAPAS FER
/**POLIGONO DE ZONA URBANA*/
function pintar_poligono_potencial_pob_urbana(){
    poligonourbana = new google.maps.Data({map: map});
    poligonourbana.loadGeoJson('veracruz/poblacion/zonas_urbanas.json'); 
    poligonourbana.setStyle (function (feature) {
      return ({
        fillOpacity: 0.7,
        strokeColor: '#FFB946',
        strokeOpacity: 0.7,
        strokeWeight: 2,
        fillColor: '#faab0b'
      });
  });
}

function apagar_poligono_potencial_pob_urbana(){
    poligonourbana.forEach(function(feature) {
        poligonourbana.remove(feature);
    });
}

function texto_poligono_potencial_pob_urbana(){
    //listen for click events
    poligonourbana.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">'+nombre+'<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
       });
}

/**capa de zona urbana*/
function checkbox_pob_urbana(){
    $("#pob_urbana").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_pob_urbana();
          ocultar_capa_veracruz();
          texto_poligono_potencial_pob_urbana();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_pob_urbana();     
        }
    });
}

//CAPAS FER

/**POLIGONO DE POTENCIAL PAPA*/
function pintar_poligono_potencial_papa(){
    poligonopapa = new google.maps.Data({map: map});
    poligonopapa.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_papa.json'); 
    var global = poligonopapa;
    mouse(global);
    poligonopapa.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FCF6DD',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#EAD686'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F3D044',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F3D044'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#CBA50C',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#CBA50C'
            });
        }
    });

}

function apagar_poligono_potencial_papa(){
    poligonopapa.forEach(function(feature) {
        poligonopapa.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_papa(){
    //listen for click events
    poligonopapa.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}


/**capa de zona papa*/
function checkbox_poten_papa(){
    $("#poten_papa").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_papa();
          ocultar_capa_veracruz();
          texto_poligono_potencial_papa();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_papa();     
        }
    });
}

/**POLIGONO DE ZONA URBANA*/
function pintar_poligono_potencial_pob_urbana(){
    poligonourbana = new google.maps.Data({map: map});
    poligonourbana.loadGeoJson('veracruz/poblacion/zonas_urbanas.json'); 
    poligonourbana.setStyle (function (feature) {
      return ({
        fillOpacity: 0.7,
        strokeColor: '#FFB946',
        strokeOpacity: 0.7,
        strokeWeight: 2,
        fillColor: '#faab0b'
      });
  });

}

function apagar_poligono_potencial_pob_urbana(){
    poligonourbana.forEach(function(feature) {
        poligonourbana.remove(feature);
    });
}

function texto_poligono_potencial_pob_urbana(){
    //listen for click events
    poligonourbana.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">'+nombre+'<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
       });
}


/**capa de zona urbana*/
function checkbox_pob_urbana(){
    $("#pob_urbana").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_pob_urbana();
          ocultar_capa_veracruz();
          texto_poligono_potencial_pob_urbana();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_pob_urbana();     
        }
    });
}

/**POLIGONO DE SUELOS PREDOMINANTES*/
function pintar_poligono_suelos_predominantes(){
    poligonoSuelosPredominantes = new google.maps.Data({map: map});
    poligonoSuelosPredominantes.loadGeoJson('veracruz/suelos/suelos_suelos_predominantes.json');
    var global = poligonoSuelosPredominantes;
    mouse(global);
    poligonoSuelosPredominantes.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Feozem háplico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de9b7a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de9b7a'
            });
        }else if(color == "Feozem calcárico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d9c09e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d9c09e'
            });
        }else if(color == "Luvisol crómico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6a25e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6a25e'
            });
        }else if(color == "Luvisol férrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f79e39',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f79e39'
            });
        }else if(color == "Luvisol órtico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5aa3b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5aa3b'
            });
        }else if(color == "Regosol dístrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bd8f5b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bd8f5b'
            });
        }else if(color == "Vertisol pélico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d9ae89',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d9ae89'
            });
        }else if(color == "Cambisol gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bd6f48',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bd6f48'
            });
        }else if(color == "Vertisol crómico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b86428',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b86428'
            });
        }else if(color == "Solonchak gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#e67d40',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e67d40'
            });
        }else if(color == "Solonchak órtico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f2b76f',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f2b76f'
            });
        }else if(color == "Cambisol cálcico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#ba8f4a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#ba8f4a'
            });
        }else if(color == "Rendzina"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d69f47',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d69f47'
            });
        }else if(color == "Regosol calcárico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bf7e45',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bf7e45'
            });
        }else if(color == "Regosol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5d4b3',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5d4b3'
            });
        }else if(color == "Cambisol crómico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#fad8a7',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#fad8a7'
            });
        }else if(color == "Gleysol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5c884',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5c884'
            });
        }else if(color == "Litosol"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6b785',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6b785'
            });
        }else if(color == "Fluvisol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f7903b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f7903b'
            });
        }else if(color == "Fluvisol gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bfa18f',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bfa18f'
            });
        }else if(color == "Acrisol húmico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#e09941',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e09941'
            });
        }else if(color == "Acrisol órtico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b57c59',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b57c59'
            });
        }else if(color == "Cambisol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f2bb9d',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f2bb9d'
            });
        }else if(color == "Cambisol vértico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#cc9f72',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#cc9f72'
            });
        }else if(color == "Gleysol mólico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#db992e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#db992e'
            });
        }else if(color == "Fluvisol calcárico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f09a54',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f09a54'
            });
        }else if(color == "Luvisol gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5ad5f',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5ad5f'
            });
        }else if(color == "Castañozem háplico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#e6b15c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e6b15c'
            });
        }else if(color == "Luvisol álbico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#cf8036',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#cf8036'
            });
        }else if(color == "Luvisol plíntico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#c9703c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#c9703c'
            });
        }else if(color == "Acrisol plíntico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#edccbb',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#edccbb'
            });
        }else if(color == "Cambisol ferrálico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f77f39',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f77f39'
            });
        }else if(color == "Gleysol vértico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d4944c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d4944c'
            });
        }else if(color == "Feozem lúvico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b36c36',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b36c36'
            });
        }else if(color == "Nitosol dístrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6b29c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6b29c'
            });
        }else if(color == "Planosol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de6a31',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de6a31'
            });
        }else if(color == "Andosol húmico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#ba5b2b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#ba5b2b'
            });
        }else if(color == "Andosol ócrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bda480',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bda480'
            });
        }else if(color == "Gleysol plíntico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6792d',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6792d'
            });
        }else if(color == "Castañozem lúvico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de9666',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de9666'
            });
        }else if(color == "Arenosol cámbico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de854e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de854e'
            });
        }else if(color == "Andosol mólico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f2b079',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f2b079'
            });
        }else if(color == "Cambisol húmico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bd8f79',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bd8f79'
            });
        }else if(color == "Cambisol dístrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b36d27',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b36d27'
            });
        }else if(color == "Luvisol vértico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#edae4a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#edae4a'
            });
        }else if(color == "Castañozem cálcico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b57d2a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b57d2a'
            });
        }
    });

}

function apagar_poligono_suelos_predominantes(){
    poligonoSuelosPredominantes.forEach(function(feature) {
        poligonoSuelosPredominantes.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_suelos_predominantes(){
    //listen for click events
    poligonoSuelosPredominantes.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">'+nombre+'<br></div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de suelos Predominantes*/
function checkbox_suelos_predominantes(){
    $("#suelos_predominantes").change(function() {
        if (this.checked) {
          pintar_poligono_suelos_predominantes();
          ocultar_capa_veracruz();
          texto_poligono_suelos_predominantes();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_suelos_predominantes();     
        }
    });
}

/**POLIGONO DE POTENCIAL ALFALFA*/
function pintar_poligono_potencial_alfalfa(){
    poligonoalfalfa = new google.maps.Data({map: map});
    poligonoalfalfa.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_alfalfa.json'); 
    var global = poligonoalfalfa;
    mouse(global);
    poligonoalfalfa.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Alto"){
          //console.log(color);
            return ({
                fillOpacity: 0.4,
                strokeColor: '#FFF700',
                strokeOpacity: 0.9,
                strokeWeight: 4,
                fillColor: '#FFF700'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#7ABB4C',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#7ABB4C'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#C2E8A7',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#C2E8A7'
            });
        }
    });

}

function apagar_poligono_potencial_alfalfa(){
    poligonoalfalfa.forEach(function(feature) {
        poligonoalfalfa.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_alfalfa(){
    //listen for click events
    poligonoalfalfa.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona alfalfa*/
function checkbox_poten_alfalfa(){
    $("#poten_alfalfa").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_alfalfa();
          ocultar_capa_veracruz();
          texto_poligono_potencial_alfalfa();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_alfalfa();     
        }
    });
}

/**POLIGONO DE POTENCIAL CEBOLLA*/
function pintar_poligono_potencial_cebolla(){
    poligonocebolla = new google.maps.Data({map: map});
    poligonocebolla.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_cebolla.json'); 
    var global = poligonocebolla;
    mouse(global);
    poligonocebolla.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#B4ED8D',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#B4ED8D'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#EEFBE5',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#EEFBE5'
            });
        }
    });

}

function apagar_poligono_potencial_cebolla(){
    poligonocebolla.forEach(function(feature) {
        poligonocebolla.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_cebolla(){
    //listen for click events
    poligonocebolla.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona cebolla*/
function checkbox_poten_cebolla(){
    $("#poten_cebolla").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_cebolla();
          ocultar_capa_veracruz();
          texto_poligono_potencial_cebolla();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_cebolla();     
        }
    });
}

/**POLIGONO DE POTENCIAL CHILE*/
function pintar_poligono_potencial_chile(){
    poligonochile = new google.maps.Data({map: map});
    poligonochile.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_chile.json'); 
    var global = poligonochile;
    mouse(global);
    poligonochile.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F33A15',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F33A15'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F9730C',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F9730C'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F7A80A',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F7A80A'
            });
        }
    });
}

function apagar_poligono_potencial_chile(){
    poligonochile.forEach(function(feature) {
        poligonochile.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_chile(){
    //listen for click events
    poligonochile.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona chile*/
function checkbox_poten_chile(){
    $("#poten_chile").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_chile();
          ocultar_capa_veracruz();
          texto_poligono_potencial_chile();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_chile();     
        }
    });
}

/**POLIGONO DE POTENCIAL COCOTERO*/
function pintar_poligono_potencial_cocotero(){
    poligonococotero = new google.maps.Data({map: map});
    poligonococotero.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_cocotero.json'); 
    var global = poligonococotero;
    mouse(global);
    poligonococotero.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.4,
                strokeColor: '#6B370E',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#6B370E'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#E89E63',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#E89E63'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#B9B5B2',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#B9B5B2'
            });
        }
    });
}

function apagar_poligono_potencial_cocotero(){
    poligonococotero.forEach(function(feature) {
        poligonococotero.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_cocotero(){
    //listen for click events
    poligonococotero.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona cocotero*/
function checkbox_poten_cocotero(){
    $("#poten_cocotero").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_cocotero();
          ocultar_capa_veracruz();
          texto_poligono_potencial_cocotero();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_cocotero();     
        }
    });
}

/**POLIGONO DE POTENCIAL FRIJOL*/
function pintar_poligono_potencial_frijol(){
    poligonofrijol = new google.maps.Data({map: map});
    poligonofrijol.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_frijol.json'); 
    var global = poligonofrijol;
    mouse(global);
    poligonofrijol.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#F6D7C0',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F6D7C0'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#EF781F',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#EF781F'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#753D14',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#753D14'
            });
        }
    });
}

function apagar_poligono_potencial_frijol(){
    poligonofrijol.forEach(function(feature) {
        poligonofrijol.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_frijol(){
    //listen for click events
    poligonofrijol.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona frijol*/
function checkbox_poten_frijol(){
    $("#poten_frijol").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_frijol();
          ocultar_capa_veracruz();
          texto_poligono_potencial_frijol();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_frijol();     
        }
    });
}

/**POLIGONO DE POTENCIAL HABA*/
function pintar_poligono_potencial_haba(){
    poligonohaba = new google.maps.Data({map: map});
    poligonohaba.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_haba.json'); 
    var global = poligonohaba;
    mouse(global);
    poligonohaba.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FFE666',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FFE666'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.4,
                strokeColor: '#F7FF00',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#F7FF00'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#BD9F0B',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#BD9F0B'
            });
        }
    });
}

function apagar_poligono_potencial_haba(){
    poligonohaba.forEach(function(feature) {
        poligonohaba.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_haba(){
    //listen for click events
    poligonohaba.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona haba*/
function checkbox_poten_haba(){
    $("#poten_haba").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_haba();
          ocultar_capa_veracruz();
          texto_poligono_potencial_haba();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_haba();     
        }
    });
}

/**POLIGONO DE POTENCIAL PALMA ACEITE*/
function pintar_poligono_potencial_palma_aceite(){
    poligonopalma_aceite = new google.maps.Data({map: map});
    poligonopalma_aceite.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_palma_aceite.json'); 
    var global = poligonopalma_aceite;
    mouse(global);
    poligonopalma_aceite.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FB8108',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FB8108'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.4,
                strokeColor: '#ECA660',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#ECA660'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FFE4C9',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FFE4C9'
            });
        }
    });
}

function apagar_poligono_potencial_palma_aceite(){
    poligonopalma_aceite.forEach(function(feature) {
        poligonopalma_aceite.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_palma_aceite(){
    //listen for click events
    poligonopalma_aceite.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona palma_aceite*/
function checkbox_poten_palma_aceite(){
    $("#poten_palma_aceite").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_palma_aceite();
          ocultar_capa_veracruz();
          texto_poligono_potencial_palma_aceite();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_palma_aceite();     
        }
    });
}

/**POLIGONO DE POTENCIAL PALMA VAINILLA*/
function pintar_poligono_potencial_vainilla(){
    poligonovainilla = new google.maps.Data({map: map});
    poligonovainilla.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_vainilla.json'); 
    var global = poligonovainilla;
    mouse(global);
    poligonovainilla.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FFE800',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FFE800'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.4,
                strokeColor: '#E8DF7D',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#E8DF7D'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#E9E6C3',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#E9E6C3'
            });
        }
    });
}

function apagar_poligono_potencial_vainilla(){
    poligonovainilla.forEach(function(feature) {
        poligonovainilla.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_vainilla(){
    //listen for click events
    poligonovainilla.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona vainilla*/
function checkbox_poten_vainilla(){
    $("#poten_vainilla").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_vainilla();
          ocultar_capa_veracruz();
          texto_poligono_potencial_vainilla();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_vainilla();     
        }
    });
}

/**POLIGONO DE POTENCIAL PALMA SORGO*/
function pintar_poligono_potencial_sorgo(){
    poligonosorgo = new google.maps.Data({map: map});
    poligonosorgo.loadGeoJson('veracruz/potencial_productivo/potencial_productivo_sorgo.json'); 
    var global = poligonosorgo;
    mouse(global);
    poligonosorgo.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if (color == "Alto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FB6C0E',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FB6C0E'
            });
        }else if (color == "Medio") {
            return ({
                fillOpacity: 0.4,
                strokeColor: '#FBC20E',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FBC20E'
            });
        }else if (color == "No apto") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#A1BF1A',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#A1BF1A'
            });
        }
    });
}

function apagar_poligono_potencial_sorgo(){
    poligonosorgo.forEach(function(feature) {
        poligonosorgo.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_potencial_sorgo(){
    //listen for click events
    poligonosorgo.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">Potencial Productivo</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de zona sorgo*/
function checkbox_poten_sorgo(){
    $("#poten_sorgo").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_sorgo();
          ocultar_capa_veracruz();
          texto_poligono_potencial_sorgo();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_sorgo();     
        }
    });
}



/**POLIGONO DE SUELOS PREDOMINANTES*/
function pintar_poligono_suelos_predominantes(){
    poligonoSuelosPredominantes = new google.maps.Data({map: map});
    poligonoSuelosPredominantes.loadGeoJson('veracruz/suelos/suelos_suelos_predominantes.json');
    var global = poligonoSuelosPredominantes;
    mouse(global);
    poligonoSuelosPredominantes.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Feozem háplico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de9b7a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de9b7a'
            });
        }else if(color == "Feozem calcárico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d9c09e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d9c09e'
            });
        }else if(color == "Luvisol crómico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6a25e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6a25e'
            });
        }else if(color == "Luvisol férrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f79e39',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f79e39'
            });
        }else if(color == "Luvisol órtico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5aa3b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5aa3b'
            });
        }else if(color == "Regosol dístrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bd8f5b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bd8f5b'
            });
        }else if(color == "Vertisol pélico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d9ae89',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d9ae89'
            });
        }else if(color == "Cambisol gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bd6f48',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bd6f48'
            });
        }else if(color == "Vertisol crómico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b86428',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b86428'
            });
        }else if(color == "Solonchak gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#e67d40',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e67d40'
            });
        }else if(color == "Solonchak órtico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f2b76f',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f2b76f'
            });
        }else if(color == "Cambisol cálcico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#ba8f4a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#ba8f4a'
            });
        }else if(color == "Rendzina"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d69f47',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d69f47'
            });
        }else if(color == "Regosol calcárico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bf7e45',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bf7e45'
            });
        }else if(color == "Regosol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5d4b3',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5d4b3'
            });
        }else if(color == "Cambisol crómico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#fad8a7',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#fad8a7'
            });
        }else if(color == "Gleysol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5c884',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5c884'
            });
        }else if(color == "Litosol"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6b785',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6b785'
            });
        }else if(color == "Fluvisol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f7903b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f7903b'
            });
        }else if(color == "Fluvisol gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bfa18f',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bfa18f'
            });
        }else if(color == "Acrisol húmico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#e09941',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e09941'
            });
        }else if(color == "Acrisol órtico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b57c59',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b57c59'
            });
        }else if(color == "Cambisol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f2bb9d',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f2bb9d'
            });
        }else if(color == "Cambisol vértico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#cc9f72',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#cc9f72'
            });
        }else if(color == "Gleysol mólico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#db992e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#db992e'
            });
        }else if(color == "Fluvisol calcárico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f09a54',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f09a54'
            });
        }else if(color == "Luvisol gléyico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f5ad5f',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f5ad5f'
            });
        }else if(color == "Castañozem háplico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#e6b15c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#e6b15c'
            });
        }else if(color == "Luvisol álbico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#cf8036',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#cf8036'
            });
        }else if(color == "Luvisol plíntico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#c9703c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#c9703c'
            });
        }else if(color == "Acrisol plíntico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#edccbb',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#edccbb'
            });
        }else if(color == "Cambisol ferrálico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f77f39',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f77f39'
            });
        }else if(color == "Gleysol vértico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d4944c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d4944c'
            });
        }else if(color == "Feozem lúvico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b36c36',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b36c36'
            });
        }else if(color == "Nitosol dístrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6b29c',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6b29c'
            });
        }else if(color == "Planosol éutrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de6a31',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de6a31'
            });
        }else if(color == "Andosol húmico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#ba5b2b',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#ba5b2b'
            });
        }else if(color == "Andosol ócrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bda480',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bda480'
            });
        }else if(color == "Gleysol plíntico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#d6792d',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#d6792d'
            });
        }else if(color == "Castañozem lúvico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de9666',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de9666'
            });
        }else if(color == "Arenosol cámbico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#de854e',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#de854e'
            });
        }else if(color == "Andosol mólico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#f2b079',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#f2b079'
            });
        }else if(color == "Cambisol húmico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#bd8f79',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#bd8f79'
            });
        }else if(color == "Cambisol dístrico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b36d27',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b36d27'
            });
        }else if(color == "Luvisol vértico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#edae4a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#edae4a'
            });
        }else if(color == "Castañozem cálcico"){
          //console.log(color);
            return ({
                fillOpacity: 0.9,
                strokeColor: '#b57d2a',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#b57d2a'
            });
        }
    });

}

function apagar_poligono_suelos_predominantes(){
    poligonoSuelosPredominantes.forEach(function(feature) {
        poligonoSuelosPredominantes.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_suelos_predominantes(){
    //listen for click events
    poligonoSuelosPredominantes.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">'+nombre+'<br></div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**POLIGONO DE POTENCIAL CARRETERAS*/
function pintar_poligono_carretera_estatal(){
    poligonocarretera = new google.maps.Data({map: map});
    poligonocarretera.loadGeoJson('veracruz/carreteras/carreteras.json'); 
    var global = poligonocarretera;
    mouse(global);
    poligonocarretera.setStyle (function (feature) {
        var color = feature.getProperty("Name");
        console.log(color);
        if(color == "Carretera Federal"){
          //console.log(color);
            return ({
                fillOpacity: 0.2,
                strokeColor: '#FECF37',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#FECF37'
            });
        }else if (color == "Carretera Estatal") {
            return ({
                fillOpacity: 0.2,
                strokeColor: '#71ac06',
                strokeOpacity: 0.9,
                strokeWeight: 2,
                fillColor: '#71ac06'
            });
        }
    });

}

function apagar_poligono_carretera_estatal(){
    poligonocarretera.forEach(function(feature) {
        poligonocarretera.remove(feature);
    });
}

/**function de el infowindow*/
function texto_poligono_carretera_estatal(){
    //listen for click events
    poligonocarretera.addListener('click', function(e) {
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">'+e.feature.getProperty("Name")+'<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
    });
}

/**capa de carreteras*/
function checkbox_carretera_estatal(){
    $("#carretera_estatal").change(function() {
        if (this.checked) {
          pintar_poligono_carretera_estatal();
          ocultar_capa_veracruz();
          texto_poligono_carretera_estatal();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_carretera_estatal();     
        }
    });
}

/**POLIGONO DE ZONA RURAL*/
function pintar_poligono_potencial_pob_rural(){
    poligonorural = new google.maps.Data({map: map});
    poligonorural.loadGeoJson('veracruz/poblacion/zonas_rurales.json');
     var global = poligonorural;
    mouse(global);
    poligonorural.setStyle (function (feature) {
      return ({
        fillOpacity: 0.2,
        strokeColor: '#BB9FD1',
        strokeOpacity: 0.7,
        strokeWeight: 2,
        fillColor: '#BB9FD1'
      });
  });

}

function apagar_poligono_potencial_pob_rural(){
    poligonorural.forEach(function(feature) {
        poligonorural.remove(feature);
    });
}

function texto_poligono_potencial_pob_rural(){
    //listen for click events
    poligonorural.addListener('click', function(e) {
         var nombre =  e.feature.getProperty("Name");
         var contenido =  e.feature.getProperty("description");
         if(info_window){
            info_window.setMap(null);
            info_window = null;
         }
         info_window = new google.maps.InfoWindow({
             content: '<div id="style_markers">'+nombre+'<br> '+'</div><br>'+contenido,
             position: e.latLng, map: map
         });
       });
}


/**capa de zona rural*/
function checkbox_pob_rural(){
    $("#pob_rural").change(function() {
        if (this.checked) {
          pintar_poligono_potencial_pob_rural();
          ocultar_capa_veracruz();
          texto_poligono_potencial_pob_rural();
        } else {
          if(info_window){
            info_window.setMap(null);
            info_window = null;
          }
          apagar_poligono_potencial_pob_rural();
        }
    });
}

