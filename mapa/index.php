<?php 
  include("views/header.php");
  include ('php/select_capas.php');
  include ('php/puntos.php');
  include ('views/archivos_externos.php');
?>

  <body>
    <!--Div de logos de gobierno-->
    <div class="logo_gob">
      <a target="_blank" href="http://www.veracruz.gob.mx/agropecuario/"><img src="img/logo-gob.png" class="imggob"></a>
    </div>
    
    <div class="btn_area">
      <!-- Default switch HERRAMIENTAS-->
      <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitches">
        <label class="custom-control-label" for="customSwitches"><i class="fa fa-cogs" style="color: white" aria-hidden="true"></i></label>
      </div>
    </div>
    
    <!--<div id="borrar_dibujo">-->
    <div class="panel_color">
      <div id="palette-color"></div>
        <div>
          <button type="button" class="btn btn-light btn-sm fa fa-trash-o" id="delete-button" title="Eliminar Herramienta"></button>
        </div>
        <div>
          <button type="button" class="btn btn-light btn-sm fa fa-print" id="imprimir-pantalla" title="Imprimir Poligono" onclick="javascript:print()"></button>
        </div>
    </div>
    <div class="ocultar_cor_medir">
        <input type="text" id="demo">
    </div>
    <!--INPUT PARA BUSCAR LUGARES EN GENERAL-->
    <div class="buscar_lugar"> 
        <input type="text" class="form-control-sm" id="pac-input" placeholder="Buscar lugar">
        <button class="btn btn-secondary btn-sm" id="eliminar-place"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </div>
    
    <!--MENÚ FLOTANTE-->
    <div id="floating-panel">
      <input type="checkbox" id="abrir-cerrar" name="abrir-cerrar" value="">
      <label for="abrir-cerrar"><span class="abrir" title="Mostrar Capas"><i class="fa fa-database" aria-hidden="true"></i></span><span class="cerrar" title="Cerrar"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span></label>
      <div id="sidebar" class="sidebar">
      <div class="container">
        <div class="">
            <label class="sr-only" for="inlineFormInputGroup"></label>
            <div class="input-group">
              <div class="input-group-prepend">
                <button class="btn btn-secondary" id="bus_mun"><i class="fa fa-search"></i></button>
              </div>
              <input type="text" class="form-control" id="tags" placeholder="Municipio">
                <button class="btn btn btn-secondary" id="delet_mun"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </div>
        </div><br>
      </div>
      <button class="accordion"><i class="fas fa-globe-americas"></i> Capas Base</button>
      <div class="panel">
        <div style="">
          <table class="table table-bordered table-sm">
            <thead>
              <tr>
                <td style="text-align: center;">
                  <input id="capa1" type="radio" name="capa" value="capa1" hidden="true" />
                  <label class="drinkcard-cc capa1" for="capa1">
                  <img style="border: 1px solid;" src="img/retro.PNG" width="40" height="40">
                  </label>
                  <span class="badge badge-light">Retro</span>
                </td>
                <td style="text-align: center;">
                  <input id="capa2" type="radio" name="capa" value="capa2" hidden="true" />
                  <label class="drinkcard-cc capa2" for="capa2">
                  <img style="border: 1px solid;" src="img/silver.PNG" width="40" height="40">
                  </label>
                  <span class="badge badge-light">Plata</span>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="text-align: center;">
                  <input id="capa3" type="radio" name="capa" value="capa3" hidden="true" />
                  <label class="drinkcard-cc capa3" for="capa3">
                  <img style="border: 1px solid;" src="img/feactures.PNG" width="40" height="40">
                  </label>
                  <span class="badge badge-light">Mapa</span>
                </td>
                <td style="text-align: center;">
                  <input id="capa4" type="radio" name="capa" value="capa4" hidden="true" />
                  <label class="drinkcard-cc capa4" for="capa4">
                  <img style="border: 1px solid;" src="img/night.PNG" width="40" height="40">
                  </label>
                  <span class="badge badge-light">Oscuro</span>
                </td>
              </tr>
              <tr>
                <td style="text-align:center;"> 
                  <input id="capa5" type="radio" name="capa" value="capa5" hidden="true" />
                  <label class="drinkcard-cc capa5" for="capa5">
                  <img style="border: 1px solid;" src="img/relieve.PNG" width="40" height="40">
                  </label>
                  <span class="badge badge-light">Relieve</span>
                </td>
                <td style="text-align:center;">
                  <input id="capa6" type="radio" name="capa" value="capa6" hidden="true" />
                  <label class="drinkcard-cc capa6" for="capa6">
                  <img style="border: 1px solid;" src="img/satelite.PNG" width="40" height="40">
                  </label>
                  <span class="badge badge-light">Satélite</span>
                </td>
              </tr>
            </tbody>
          </table> 
        </div> 
        <div class="form-group formest">
          <input type="checkbox" id="ver" checked><span class="label label-default"> Veracruz</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" id="municipios"><span class="label label-default"> Municipios</span>
        </div>  
      </div>

      <button class="accordion"><i class="fa fa-info"></i> Centros de Atención (SADER)</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="ofi_cader" id="ofi_cader"><span class="label label-default"> Oficinas CADER</span> 
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="limites_cader"><span class="label label-default"> Limites CADER</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="ofi_ddr" id="ofi_ddr"><span class="label label-default"> Oficinas DDR</span> 
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="limites_ddr"><span class="label label-default"> Limites DDR</span>  
        </div>
      </div>
      <button class="accordion"><i class="fab fa-pagelines"></i> Agricultura</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="agri_protegida" id="agri_protegida"><span class="label label-default"> Agricultura Protegida</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="almg"><span class="label label-default"> Almacenes de granos</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="benef"><span class="label label-default"> Beneficios de café</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="ing_azu"><span class="label label-default"> Ingenieros azucareros</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="cobertura_ing"><span class="label label-default"> Cobertura de Ingenios</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="centroa"><span class="label label-default"> Centros de Acopio - SEGALMEX</span>  
        </div>
      </div>

      <button class="accordion"><i class="fab fa-buffer"></i> Ganadería</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" class="" id="insps"><span class="label label-default"> Inspección Sanitaria</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="sacrif_proc"><span class="label label-default"> Establecimientos TIF</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="tifmunicipal"><span class="label label-default"> Centros de Sacrificio</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="establos"><span class="label label-default"> Establos lecheros</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="corrales"><span class="label label-default"> Corrales de engorda</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="granjas_ave"><span class="label label-default"> Granja de ave</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" class="" id="granjas_huevo"><span class="label label-default"> Granja de huevo</span>
      </div>
      </div>


      <button class="accordion"><i class="fa fa-ship"></i> Pesca</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="" id="unit_pesca"><span class="label label-default"> Unidad de pesca</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="puertos"><span class="label label-default"> Puertos de Veracruz</span>
        </div>
      </div>

      <button class="accordion"><i class="fa fa-yelp" aria-hidden="true"></i> Potencial Productivo</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_cacao"><span class="label label-default"> Cacao</span>
            <div id="paleta_cacao">
              <table>
                <tr>
                  <td><div><span id="altocacao" class="legend-color-box"></span>Alto</div></td>
                </tr>
                <tr>
                  <td><div><span id="mediocacao" class="legend-color-box"></span>Medio</div></td>
                </tr>
                <tr>
                  <td><div><span id="noaptocacao" class="legend-color-box"></span>No apto</div></td>
                </tr>
              </table>
            </div>
        </div>

        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_cana"><span class="label label-default"> Caña de azucar</span>
            <div id="paleta_cana">
              <table>
                <tr>
                  <td><div><span id="altocana" class="legend-color-box"></span>Alto</div></td>
                </tr>
                <tr>
                  <td><div><span id="mediocana" class="legend-color-box"></span>Medio</div></td>
                </tr>
                <tr>
                  <td><div><span id="noaptocana" class="legend-color-box"></span>No apto</div></td>
                </tr>
              </table>
            </div>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_hule"><span class="label label-default"> Hule</span>
            <div id="paleta_hule">
              <table>
                <tr>
                  <td><div><span id="altohule" class="legend-color-box"></span>Alto</div></td>
                </tr>
                <tr>
                  <td><div><span id="mediohule" class="legend-color-box"></span>Medio</div></td>
                </tr>
                <tr>
                  <td><div><span id="noaptohule" class="legend-color-box"></span>No apto</div></td>
                </tr>
              </table>
            </div>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_maiz"><span class="label label-default"> Maíz Grano</span>
            <div id="paleta_maiz">
              <table>
                <tr>
                  <td><div><span id="altomaiz" class="legend-color-box"></span>Alto</div></td>
                </tr>
                <tr>
                  <td><div><span id="mediomaiz" class="legend-color-box"></span>Medio</div></td>
                </tr>
                <tr>
                  <td><div><span id="noaptomaiz" class="legend-color-box"></span>No apto</div></td>
                </tr>
              </table>
            </div>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_naranja"><span class="label label-default"> Naranja</span>
            <div id="paleta_naranja">
              <table>
                <tr>
                  <td><div><span id="altonaranja" class="legend-color-box"></span>Alto</div></td>
                </tr>
                <tr>
                  <td><div><span id="medionaranja" class="legend-color-box"></span>Medio</div></td>
                </tr>
                <tr>
                  <td><div><span id="noaptonaranja" class="legend-color-box"></span>No apto</div></td>
                </tr>
              </table>
            </div>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_pasto_estrella"><span class="label label-default"> Pasto estrella</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_platano"><span class="label label-default"> Plátano</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_mango"><span class="label label-default"> Mango</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_jitomate"><span class="label label-default"> Jitomate</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_trigo"><span class="label label-default"> Trigo</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="rendimiento_aguacate"><span class="label label-default"> Aguacate</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="rendimiento_cafe"><span class="label label-default"> Café</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="limon_persa"><span class="label label-default"> Limón persa</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="ajo"><span class="label label-default"> Ajo</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="avena"><span class="label label-default"> Avena</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="pimienta"><span class="label label-default"> Pimienta</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_alfalfa"><span class="label label-default"> Alfalfa</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_cebolla"><span class="label label-default"> Cebolla</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_chile"><span class="label label-default"> Chile</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_cocotero"><span class="label label-default"> Coco</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_frijol"><span class="label label-default"> Frijol</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_haba"><span class="label label-default"> Haba</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_palma_aceite"><span class="label label-default"> Palma de aceite</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_papa"><span class="label label-default"> Papa</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_sorgo"><span class="label label-default"> Sorgo Grano</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="poten_vainilla"><span class="label label-default"> Vainilla</span>
        </div>
      </div>

       <button class="accordion"><i class="fa fa-pied-piper-alt"></i> Suelo</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="" id="vegetacion"><span class="label label-default"> Uso de suelo y vegetación</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="suelos_predominantes"><span class="label label-default"> Suelos predominantes </span>
        </div>
      </div>

      <button class="accordion"><i class="fa fa-thermometer-full"></i> Sequía</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="sequia2019" id="sequia_2019"><span class="label label-default"> Sequía 2019</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="covertura_enero"><span class="label label-default"> Covertura 2020</span>
        </div>
        <div class="form-group formest text-center">
        <span class="badge badge-light">Avances de sequías 2020</span>
          <button type="button" style="border:1px solid rgba(141, 110, 14, 0.80);" class="btn btn-sm btn-block" data-toggle="modal" data-target="#exampleModalCenter">Año 2020</button>
        </div>
      </div>

      <button class="accordion"><i class="fa fa-tint"></i> Hidrografía</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" class="" id="capa_presas"><span class="label label-default"> Presas</span>  
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="pozos_perote"><span class="label label-default"> Pozos (Perote)</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="reg_hidro"><span class="label label-default"> Regiones Hidrólogicas</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="dis_riego"><span class="label label-default"> Distritos de Riego</span>
        </div>
      </div>

      <button class="accordion"><i class="fa fa-lemon-o"></i> Proyectos de Interés Estatal</button>
      <div class="panel">
        <span class="badge badge-light">Estimación de Superficies</span>
        <div class="form-group formest">
          <input type="checkbox" name="" id="citrico_tlal"><span class="label label-default"> Limón (Tlaltetela)</span>
        </div>
        <div class="form-group formest">
          <input type="checkbox" name="" id="chayote_actopan"><span class="label label-default"> Chayote (Actopan)</span>
        </div>
        <span class="badge badge-light">Vuelos de DRONE</span>
        <div class="form-group formest">
          <input type="checkbox" name="" id="raster_actopan"><span class="label label-default"> Otates (Actopan)</span>
        </div>
      </div>

      <button class="accordion"><i class="fa fa-users"></i> Poblacion</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" name="" id="pob_urbana"><span class="label label-default"> Zonas Urbanas</span>
        </div>
        <div class="form-group">
          <input type="checkbox" name="" id="pob_rural"><span class="label label-default"> Zonas Rurales</span>
        </div>

      </div>

      <button class="accordion"><i class="fa fa-exchange"></i> Vías de Comunicación</button>
      <div class="panel">
        <div class="form-group formest">
          <input type="checkbox" id="carretera_estatal"><span class="label label-default"> Red de Carreteras</span>
        </div> 
      </div>

      </div>
    </div>

    <div id="map"></div>
    <!--<div id="legend-container">
        <center><h6><strong>Sequias</strong></h6></center>
        <table>
          <tr>
            <td><div><span id="seco" class="legend-color-box"></span>Anormalmente seco</div></td>
          </tr>
          <tr>
            <td><div><span id="moderada" class="legend-color-box"></span>sequia moderada</div></td>
          </tr>
          <tr>
            <td><div><span id="severa" class="legend-color-box"></span>sequia severa</div></td>
          </tr>
          <tr>
            <td><div><span id="extrema" class="legend-color-box"></span>sequia extrema</div></td>
          </tr>
          <tr>
            <td><div><span id="excepcional" class="legend-color-box"></span>sequia excepcional</div></td>
          </tr>
        </table>  
      </div>-->
    
    <script type="text/javascript">  
    busc_municicpios();
    //Variable de marcador de poligono de medición
    var markers = [1];
    //Variable de referencia
    var demodiv;
    var p;
    var drawingManager;
    var selectedShape;
    var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
    var selectedColor;
    var colorButtons = {};
    //variable para show and hide los markers  
    var mar = [];
    var proc =[];
    var almg = [];
    var benef = [];
    var centroac = [];
    var azucarera = [];
    var pesca = [];
    var eliminarmedir = [];
    /**array de sequias*/
    var sequia1 = [];
    var sequia2 = [];
    var sequia3 = [];
    /**array de tif municipal*/
    var tif_municipal = [];
    /**array establos lecheros*/
    var establos = [];
    /**array de corrales de engorda*/
    var corrales = [];
    //variable para mostrar la ventana de información.
    var infowindow;
    //variables para eliminar el marcador de cada municipio al buscar un municipio
    var marcado = [1];
    var marcad = [];
    //Variable de marcadores de lugares
    var placeMarkers = [];
    /**array de granjas de aves*/
    var ave = [];
    //Array de presas
    var presas = []; 
    //Array de oficinas cader
    var oficader = [];
    //Array de oficinas ddr
    var ofiddr = [];
    /**array de granjas de huevo*/
    var huevo = [];
    /**array de pozos perote*/
    var pozos = [];
    /**array de puertos de veracruz*/
    var puerto = []; 

    //Variable de infowindows de capa de poligonos
    var info_window;
    //function donde se declaran las variables para los poligonos de los municipios.
    variables_mun();  
    function initMap() {
    //MAPAS BASE
    mapas_base();
    //OCULTAR DIV DE COORDENADAS DE MEDIR
    $('.ocultar_cor_medir').hide();
    //OCULTAR DIV DE BUSQUEDAD DE LUAGRES
    $('.buscar_lugar').hide();
    //MAP PRINCIPAR
		map = new google.maps.Map(document.getElementById('map'), {
	    center: {lat: 20.13042184, lng: -96.8188441},
	    zoom: 6.8,
      mapTypeControl: false,
      mapTypeControlOptions: {
          mapTypeIds: ['roadmap', 'satellite', 'styled_map','terrain', 'silver_map', 'oscuro_map']
      },
      /*mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_LEFT
      },*/
      zoomControl: true,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
      },
      fullscreenControl: true,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
      },
      scaleControl: true,
      streetViewControl: true,
      streetViewControlOptions: {
          position: google.maps.ControlPosition.BOTTOM_RIGTH
      }

      /*disableDefaultUI: true,
      zoomControl: true*/
	    });
      map.setMapTypeId('satellite');
      /*capas_municipios();
      //capa para veracruz*/
      
      //color_agri_pro();
      /*funciones para la capa de poligono sequias 2019*/
      //checkbox_sequia2019();
      //agregar_panel_leyenda_sequias2019();
      ///////////////

      ////////////////
      
      capa_veracruz();
      /*capas();*/
      /*color();*/
      
      /*herramientas();
      mostrar_herramientas();*/
      mostrar_capa_inspeccion_sanitaria();
      mostrar_capa_sacrificio_y_procesamiento();
      mostrar_capa_almacen_granos();
      
      mostrar_capa_beneficio_cafe();
      mostrar_capa_centro_acopio();
      //ingenios azucareros
      mostrar_capa_ingenieros_azucareros();
      //unidad pesca
      mostrar_unidad_pesca();

      
      //CAPAS DE POLIGONOS
      /*funciones para la capa de poligono sequias 2019*/
      checkbox_sequia2019()
      /**function que pinta el poligono de covertura de sequia enero 2020*/
      checkbox_covertura_sequia_enero_2020();
      agregar_panel_leyenda_sequias2019();
      mostrar_sequias_enero_15_2020();
      mostrar_sequias_enero_31_2020();
      mostrar_sequias_febrero_15_2020();
      /**funciones de centros de sacricifio municicpal*/
      mostrar_rastros_tif_municipal();
      /**function para establos lecheros*/
      mostrar_establos_lecheros();
      /**function para corrales de engorda*/
      mostrar_corrales_de_engorda();
      //FUNCIÓN DE MOSTRAR CAPA DE AGRICULTURA PROT
      mostrar_capa_agri_pro();
      //FUNCIÓN PARA MOSTRAR CAPA DE COBERTURA URBANA
      mostrar_capa_cobertura_ing();
      //FUNCIÓN PARA MOSTRAR CAPA DE REGIONES HIDROLOGICAS
      mostrar_capa_reg_hidro();
      //FUNCIÓN PARA MOSTRAR CAPA DE DISTRITOS EN RIEGO
      mostrar_capa_dis_riego();
      /**function para mostra granjas de aves 2019*/
      mostrar_granjas_de_ave_2019();
      /**function mostrar granjas de huevo*/
      mostrar_granjas_de_huevo_2019();
      //CITRICOS TLALTETELA
      checkbox_citricos_tlaltetela();
      //MOSTRAR CAPAS DE PRESAS
      mostrar_capa_presas();
      //MOSTRAR CAPA DE OFICINAS CADER
      mostrar_capa_oficader();
      //MOSTRAR CAPA DE OFICINAS DDR
      mostrar_capa_ofiddr();
      /**function que muestra el poligono de limites CADER 2019*/
      checkbox_limites_cader();
      /**function que muestra el poligono de limites ddr 2019*/
      checkbox_limites_ddr();
      //MOSTRAR CAPAS DE POTENCIAL CACAO
      checkbox_poten_cacao();
      //MOSTRAR CAPAS DE POTENCIAL CAÑA
      checkbox_poten_cana();
      //MOSTRAR CAPA DE POTENCIAL HULE
      checkbox_poten_hule();
      //MOSTRAR CAPA DE POTENCIAL MAIZ
      checkbox_poten_maiz();
      //MOSTRAR CAPA DE POTENCIAL NARANJA
      checkbox_poten_naranja();
      //MOSTRAR CAPA DE POTENCIAL PASTO ESTRELLA
      checkbox_poten_pasto_estrella();
      //MOSTRAR CAPA DE POZOS PEROTE
      mostrar_puntos_pozos();
      //MOSTRAR CAPA DE PLATANO
      checkbox_poten_platano();
      //MOSTRAR CAPA DE MANGO
      checkbox_poten_mango();
      //MOSTRAR CAPA DE JITOMATE
      checkbox_poten_jitomate();
      //MOSTRAR CAPA DE TRIGO
      checkbox_poten_trigo();
      
      //CAPAS ROGER
      /**function que pinta el poligono de potencial pimienta*/
      checkbox_potencial_pimienta();
       /**function que pinta el poligono de potencial de avena*/
      checkbox_potencial_avena();
       /**function que pinta el poligono de potencial de ajo*/
      checkbox_potencial_ajo();
       /**function para apagar el poligono de Vegetacion y Suelo*/
      checkbox_potencial_vegetacion_suelo();
       /**function para apagar el poligono de potencial de cafe*/
      checkbox_potencial_cafe();
       /**function que pinta el poligono de potencial de limon persa*/
      checkbox_potencial_limon_persa();
       /**function que pinta el poligono de potencial de aguacate*/
      checkbox_potencial_aguacate();
      //CAPAS FER
      //MOSTRAR CAPA ZONAS URBANAS
      checkbox_pob_urbana();
      //MOSTRAR CAPAS DE POTENCIAL ALFALFA
      checkbox_poten_alfalfa(); 
      //MOSTRAR CAPAS DE POTENCIAL CEBOLLA
      checkbox_poten_cebolla();  
      //MOSTRAR CAPAS DE POTENCIAL chile
      checkbox_poten_chile(); 
      //MOSTRAR CAPAS DE POTENCIAL COCOTERO
      checkbox_poten_cocotero();   
      //MOSTRAR CAPA DE POTENCIAL FRIJOL
      checkbox_poten_frijol();
      //MOSTRAR CAPA DE POTENCIAL HABA
      checkbox_poten_haba();
      //MOSTRAR CAPA DE POTENCIAL PALMA DE ACEITE
      checkbox_poten_palma_aceite();
      //MOSTRAR CAPA DE POTENCIAL SORGO
      checkbox_poten_sorgo();
      //MOSTRAR CAPA ZONAS VAINILLA
      checkbox_poten_vainilla();
      //MOSTRAR SUELOS PREDOMINANTES
      checkbox_suelos_predominantes();
      //MOSTRAR CAPA DE PAPA
      checkbox_poten_papa();
      //MOSTRAR CAPA DE CARRETERA
      checkbox_carretera_estatal();
       /**function que muestra la capa de puertos de veracruz*/
      mostrar_puertos_veracruz();
      //MOSTRAR CAPA ZONAS RURAL
      checkbox_pob_rural();
      //MOSTRAR CAPA DE CHAYOTE ACTOPAN
      checkbox_chayote_actopan();
      

      /*OCULTAR PALETAS DE POTENCIAL PRODUCTIVO*/
      //CACAO
      $('#paleta_cacao').hide(); 
      //CAÑA
      $('#paleta_cana').hide(); 
      //HULE
      $('#paleta_hule').hide(); 
      //MAIZ
      $('#paleta_maiz').hide();
      //NARANJA
      $('#paleta_naranja').hide();


      //Barra de herramientas
      mostrar_herramientas();
      barra_her();
      $('.panel_color').hide();
      /*Oculta la barra de herramientas*/
      drawingManager.setOptions({
        drawingControl: false
      });
      //Función de input de buscar lugares
      searchboxplaces();
		}
    ///Desplegar menu///
    desplegar_menu();

     /////////termina function init map
     //Esta function nos permite pintar las capas de los municipios.
    /*function eqfeed_callback(results) {
        map.data.addGeoJson(results);
    }*/
 


    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKrV88GHRaVGDBAmgj2_KmGnFnFOl4zvs&libraries=drawing,places&callback=initMap"
    async defer></script>
  </body>
</html>