	/*$(document).ready(function(){
		$("#mostrar").on( "click", function() {
			$('#target').show(); //muestro mediante id
			$('.target').show(); //muestro mediante clase
		 });
		$("#ocultar").on( "click", function() {
			$('#target').hide(); //oculto mediante id
			$('.target').hide(); //muestro mediante clase
		});
	});*/
	//FUNCIÓN PARA MOSTRAR Y OCULTAR BARRA DE HERRAMIENTAS
	function mostrar_herramientas(){
  	$("#customSwitches").on('change',function(){
	    if ($(this).is(':checked') ) {
	       //mostrar div
	       $('.panel_color').show();
	       $('.buscar_lugar').show();
	       //mostrar barra de herramientas
	       drawingManager.setOptions({
		   drawingControl: true
		   });
	      
	    }else{
	        //ocultar div
	        $('.panel_color').hide();
	        $('.buscar_lugar').hide();
	        /*Oculta la barra de herramientas*/
	  		drawingManager.setOptions({
	    		drawingControl: false
	  		});
	    }
    });
	}    
	/*Función para crear barra de herramientas*/
	function barra_her(){
	  //Devuelve una referencia al elemento por su ID.
	  demodiv = document.getElementById('demo');
	  var polyOptions = {
	    strokeWeight: 0,
	    fillOpacity: 0.45,
	    editable: true
	};

      // Creates a drawing manager attached to the map that allows the user to draw
      // markers, lines, and shapes.
      drawingManager = new google.maps.drawing.DrawingManager({
        /*drawingMode: google.maps.drawing.OverlayType.POLYGON,*/
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [
            google.maps.drawing.OverlayType.POLYGON,
            google.maps.drawing.OverlayType.POLYLINE,
            google.maps.drawing.OverlayType.CIRCLE,
            google.maps.drawing.OverlayType.RECTANGLE,
            google.maps.drawing.OverlayType.MARKER
          ]
        },
        markerOptions: {
          /*draggable: true,*/
          editable: true,
        },
        polylineOptions: {
        	
          editable: true,
        },
        rectangleOptions: polyOptions,
        circleOptions: polyOptions,
        polygonOptions: polyOptions,
        
        map: map
      });

      google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
          //~ if (e.type != google.maps.drawing.OverlayType.MARKER) {
            var isNotMarker = (e.type != google.maps.drawing.OverlayType.MARKER);
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);
            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            google.maps.event.addListener(newShape, 'click', function() {
              setSelection(newShape, isNotMarker);
            });
            google.maps.event.addListener(newShape, 'drag', function() {
              updateCurSelText(newShape);
            });
            /*google.maps.event.addListener(newShape, 'dragend', function() {
              updateCurSelText(newShape);
            });*/
            //FUNCIÓN PARA CALCULAR EL ÁREA DEL POLIGONO DIBUJADO
             var area = google.maps.geometry.spherical.computeArea(newShape.getPath());
	      /*document.getElementById("area").innerHTML = "Area =" + area;*/
            setSelection(newShape, isNotMarker);
          //~ }// end if
        });



      // Clear the current selection when the drawing mode is changed, or when the
      // map is clicked.
      google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
      google.maps.event.addListener(map, 'click', clearSelection);
      google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', deleteSelectedShape);

      buildColorPalette();
     
    
    google.maps.event.addDomListener(window, 'load');

    }

	function clearSelection() {
	    if (selectedShape) {
	      if (typeof selectedShape.setEditable == 'function') {
	        selectedShape.setEditable(false);
	      }
	      selectedShape = null;
	    }
	}

	//FUNCIÓN PARA  OBTENER LAS POSICIONES DEL POLIGONO DIBUJADO
	function updateCurSelText(shape) {
        posstr = "" + selectedShape.position;
        if (typeof selectedShape.position == 'object') {
          posstr = selectedShape.position.toUrlValue();
        }
        pathstr = "" + selectedShape.getPath;
        if (typeof selectedShape.getPath == 'function') {
          pathstr = "[ ";
          for (var i = 0; i < selectedShape.getPath().getLength(); i++) {
            // .toUrlValue(5) limits number of decimals, default is 6 but can do more
            pathstr += selectedShape.getPath().getAt(i).toUrlValue() + " , ";
          }
          pathstr += "]";
        }
        //IMPRIME LATITUD Y LONGITUD
        p = selectedShape.getPath().getAt(0,1);
        /*eje(p);*/
        $('#demo').val(p);
    }
	//

    function marcador_area(area){
	    var d = $('#demo').val();
	    var cadena1 = d;
	    var cadena2 = cadena1.slice(1, -1);
	    var coordenadas = cadena2.split(',');

	    console.log(area);
	    //RECORTA DECIMALES
	    var ndecimal = area.toFixed(2);
	    //CONVIERTE A CADENA DE TEXTO 
		var n = ndecimal.toString();

	    for (var i = 0; i < markers.length; i++) {
	    	var position = new google.maps.LatLng(coordenadas[0],coordenadas[1]);
	          //bounds.extend(position);
	          marker = new google.maps.Marker({
	              position: position,
	              draggable: true,
	              /*icon: 'img/icono-inspeccionn.png',*/
	              map: map,
	              /*title: "Superficie: "+ n +" m2"*/
	               icon: {
			      url: "img/icono-medir.png",
			      labelOrigin: new google.maps.Point(75, 38),
			      size: new google.maps.Size(32,32),
			      anchor: new google.maps.Point(16,32)
			    },
			    label: {
			      text:"Superficie: "+ n +" m",
			      color: "#FFFFFF",
			      fontWeight: "bold"
			    }
	          });
	          eliminarmedir.push(marker);
	
	    }
	    
      }
    function eliminAll(map) {
	    for (var i = 0; i < eliminarmedir.length; i++) {
	    eliminarmedir[i].setMap(map);
    	}
	}
	function limar() {
	    eliminAll(null);
	}

	function eliminarnat() {
	    eliminAll(map);
	} 
	//FUNCIÓN PARA SELECCIONAR POLIGONO Y EDITARLO
	function setSelection(shape, isNotMarker) {
	    clearSelection();
	    selectedShape = shape;
	    if (isNotMarker){ 
	    shape.setEditable(true);
	    selectColor(shape.get('fillColor') || shape.get('strokeColor'));
		/*google.maps.event.addListener(shape.getPath(), 'set_at', calcar);
  		google.maps.event.addListener(shape.getPath(), 'insert_at', calcar);*/
  		///MARCADOR
  		updateCurSelText(shape);
  		var area = google.maps.geometry.spherical.computeArea(selectedShape.getPath());
  		/*console.log(area);
  		var area="area";*/
  		limar();
       	marcador_area(area);
       
  		}
	}

    function deleteSelectedShape() {
	    if (selectedShape) {
	      selectedShape.setMap(null);
	      limar();
	    }
    }

	function selectColor(color) {
        selectedColor = color;
        for (var i = 0; i < colors.length; ++i) {
          var currColor = colors[i];
          colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
	}

      // Retrieves the current options from the drawing manager and replaces the
      // stroke or fill color as appropriate.
      var polylineOptions = drawingManager.get('polylineOptions');
      polylineOptions.strokeColor = color;
      drawingManager.set('polylineOptions', polylineOptions);
     

      var rectangleOptions = drawingManager.get('rectangleOptions');
      rectangleOptions.fillColor = color;
      drawingManager.set('rectangleOptions', rectangleOptions);

      var circleOptions = drawingManager.get('circleOptions');
      circleOptions.fillColor = color;
      drawingManager.set('circleOptions', circleOptions);

      var polygonOptions = drawingManager.get('polygonOptions');
      polygonOptions.fillColor = color;
      drawingManager.set('polygonOptions', polygonOptions);
    }

    function setSelectedShapeColor(color) {
      if (selectedShape) {
        if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
          selectedShape.set('strokeColor', color);
        } else {
          selectedShape.set('fillColor', color);
        }
      }
    }

    function makeColorButton(color) {
      var button = document.createElement('span');
      button.className = 'color-button';
      button.style.backgroundColor = color;
      google.maps.event.addDomListener(button, 'click', function() {
        selectColor(color);
        setSelectedShapeColor(color);
       

      });

      return button;
    }

    function buildColorPalette() {
      var colorPalette = document.getElementById('palette-color');
      for (var i = 0; i < colors.length; ++i) {
        var currColor = colors[i];
        var colorButton = makeColorButton(currColor);
        colorPalette.appendChild(colorButton);
        colorButtons[currColor] = colorButton;
      }
      selectColor(colors[0]);
    }
    /////////////////////////////////////////
    
    /*Función para subir y bajar el menu*/
    function desplegar_menu(){
    	var acc = document.getElementsByClassName("accordion");
	    var i;

	    for (i = 0; i < acc.length; i++) {
	      acc[i].addEventListener("click", function() {
	        this.classList.toggle("active");
	        var panel = this.nextElementSibling;
	        if (panel.style.maxHeight) {
	          panel.style.maxHeight = null;
	        } else {
	          panel.style.maxHeight = panel.scrollHeight + "px";
	        } 
	      });
    }
    }
    //Función para eliminar y limpiar marcadores de lugares
    function deletePlacesSearchResults() {
        for (var i = 0, marker_place; marker_place = placeMarkers[i]; i++) {
          marker_place.setMap(null);
        }
        placeMarkers = [];
        input.value = ''; // clear the box too
      }

    /*Función para el input de busquedad de lugares*/
    function searchboxplaces(){
         // Create the search box and link it to the UI element.
        input = (document.getElementById('pac-input'));
        var DelPlcButDiv = document.getElementById('eliminar-place');
        
        google.maps.event.addDomListener(DelPlcButDiv, 'click', deletePlacesSearchResults);
        searchBox = new google.maps.places.SearchBox((input));
        // Listen for the event fired when the user selects an item from the
        // pick list. Retrieve the matching places for that item.
        google.maps.event.addListener(searchBox, 'places_changed', function() {
          var places = searchBox.getPlaces();
          if (places.length == 0) {
            return;
          }
          for (var i = 0, marker_place; marker_place = placeMarkers[i]; i++) {
            marker_place.setMap(null);
          }
          // For each place, get the icon, place name, and location.
          placeMarkers = [];
          var bounds = new google.maps.LatLngBounds();
          for (var i = 0, place; place = places[i]; i++) {
            var image = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };
            // Create a marker for each place.
            var marker_place = new google.maps.Marker({
              map: map,
              icon: image,
              title: place.name,
              position: place.geometry.location
             
            });
            placeMarkers.push(marker_place);
            bounds.extend(place.geometry.location);
          }
          map.fitBounds(bounds);
        });
    }

    //Función para imprimir pantalla
    function imprimir(){
	  var objeto=document.getElementById('map');  //obtenemos el objeto a imprimir
	  var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
	  ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
	  ventana.document.close();  //cerramos el documento
	  ventana.print();  //imprimimos la ventana
	  ventana.close();  //cerramos la ventana
	}

	/*Función para la barra de herramientas
	function herramientas(){
		var drawingManager = new google.maps.drawing.DrawingManager({
          /*Propiedad del dibujo principal
          drawingMode: google.maps.drawing.OverlayType.MARKER,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
          },
          markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
          circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
          }
        });
        drawingManager.setMap(map);
	}
	//el color se obtiene de cada json de los estados llamado color
	/*function color(){
		// Añadir algún estilo.
		  map.data.setStyle (function (feature) {
		    return ({
		      fillColor: feature.getProperty ('color'),
		      strokeWeight: 1
		    });
		  });

	}*/

	/*function capas(){
		var scri = document.createElement('script');
	        scri.src = 'veracruz/ACAJETE.json';
	        document.getElementsByTagName('head')[0].appendChild(scri);

	    var m = document.createElement('script');
	        m.src = 'veracruz/XALAPA.json';
	        document.getElementsByTagName('head')[0].appendChild(m);

	    /*CAPAS ROGER*/
	    /*var act = document.createElement('script');
	        act.src = 'veracruz/ACTOPAN.json';
	        document.getElementsByTagName('head')[0].appendChild(act);

	    var aca = document.createElement('script');
	        aca.src = 'veracruz/ACAYUCAN.json';
	        document.getElementsByTagName('head')[0].appendChild(aca);

	    var agua = document.createElement('script');
	        agua.src = 'veracruz/AGUA_DULCE.json';
	        document.getElementsByTagName('head')[0].appendChild(agua);

	    var alamo = document.createElement('script');
	        alamo.src = 'veracruz/ALAMO_TEMAPACHE.json';
	        document.getElementsByTagName('head')[0].appendChild(alamo);

	    var altotong = document.createElement('script');
	        altotong.src = 'veracruz/ALTOTONGA.json';
	        document.getElementsByTagName('head')[0].appendChild(altotong);

	    var alvarado = document.createElement('script');
	        alvarado.src = 'veracruz/ALVARADO.json';
	        document.getElementsByTagName('head')[0].appendChild(alvarado);

	    var amatitlan = document.createElement('script');
	        amatitlan.src = 'veracruz/AMATITLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(amatitlan);

	    var amatlan = document.createElement('script');
	        amatlan.src = 'veracruz/AMATLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(amatlan);

	    var angelr = document.createElement('script');
	        angelr.src = 'veracruz/ANGELR.json';
	        document.getElementsByTagName('head')[0].appendChild(angelr);

	    var apazapan = document.createElement('script');
	        apazapan.src = 'veracruz/APAZAPAN.json';
	        document.getElementsByTagName('head')[0].appendChild(apazapan);

	    var astacinga = document.createElement('script');
	        astacinga.src = 'veracruz/ASTACINGA.json';
	        document.getElementsByTagName('head')[0].appendChild(astacinga);

	    var atla = document.createElement('script');
	        atla.src = 'veracruz/ATLAHUILCO.json';
	        document.getElementsByTagName('head')[0].appendChild(atla);

	    var atzacan = document.createElement('script');
	        atzacan.src = 'veracruz/ATZACAN.json';
	        document.getElementsByTagName('head')[0].appendChild(atzacan);

	    var ayahual = document.createElement('script');
	        ayahual.src = 'veracruz/AYAHUALULCO.json';
	        document.getElementsByTagName('head')[0].appendChild(ayahual);

	    var camerino = document.createElement('script');
	        camerino.src = 'veracruz/CAMERINO_MENDOZA.json';
	        document.getElementsByTagName('head')[0].appendChild(camerino);

	     var carlos = document.createElement('script');
	        carlos.src = 'veracruz/CARLOS_CARRILLO.json';
	        document.getElementsByTagName('head')[0].appendChild(carlos);

	    var carrillo = document.createElement('script');
	        carrillo.src = 'veracruz/CARRILLO_PUERTO.json';
	        document.getElementsByTagName('head')[0].appendChild(carrillo);

	    var castillo = document.createElement('script');
	        castillo.src = 'veracruz/CASTILLO_DE_TEAYO.json';
	        document.getElementsByTagName('head')[0].appendChild(castillo);

	    var catemaco = document.createElement('script');
	        catemaco.src = 'veracruz/CATEMACO.json';
	        document.getElementsByTagName('head')[0].appendChild(catemaco);

	    var cazones = document.createElement('script');
	        cazones.src = 'veracruz/CAZONES_DE_HERRERA.json';
	        document.getElementsByTagName('head')[0].appendChild(cazones);

	    var cerro = document.createElement('script');
	        cerro.src = 'veracruz/CERRO_AZUL.json';
	        document.getElementsByTagName('head')[0].appendChild(cerro);

	    var chacal = document.createElement('script');
	        chacal.src = 'veracruz/CHACALTIANGUIS.json';
	        document.getElementsByTagName('head')[0].appendChild(chacal);

	    var chalma = document.createElement('script');
	        chalma.src = 'veracruz/CHALMA.json';
	        document.getElementsByTagName('head')[0].appendChild(chalma);

	    var chiconamel = document.createElement('script');
	        chiconamel.src = 'veracruz/CHICONAMEL.json';
	        document.getElementsByTagName('head')[0].appendChild(chiconamel);

	    var chinampa = document.createElement('script');
	        chinampa.src = 'veracruz/CHINAMPA.json';
	        document.getElementsByTagName('head')[0].appendChild(chinampa);

	    var chumatlan = document.createElement('script');
	        chumatlan.src = 'veracruz/CHUMATLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(chumatlan);

	    var citlal = document.createElement('script');
	        citlal.src = 'veracruz/CITLALTEPETL.json';
	        document.getElementsByTagName('head')[0].appendChild(citlal);

	    var coahutl = document.createElement('script');
	        coahutl.src = 'veracruz/COAHUITLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(coahutl);

	    var coetzala = document.createElement('script');
	        coetzala.src = 'veracruz/coetzala.json';
	        document.getElementsByTagName('head')[0].appendChild(coetzala);

	    var comapa = document.createElement('script');
	        comapa.src = 'veracruz/COMAPA.json';
	        document.getElementsByTagName('head')[0].appendChild(comapa);

	    var cosamaloapan = document.createElement('script');
	        cosamaloapan.src = 'veracruz/cosamaloapan.json';
	        document.getElementsByTagName('head')[0].appendChild(cosamaloapan);

	    var cosolea = document.createElement('script');
	        cosolea.src = 'veracruz/Cosoleacaque.json';
	        document.getElementsByTagName('head')[0].appendChild(cosolea);

	    var coyutla = document.createElement('script');
	        coyutla.src = 'veracruz/COYUTLA.json';
	        document.getElementsByTagName('head')[0].appendChild(coyutla);

	    var emiliano = document.createElement('script');
	        emiliano.src = 'veracruz/EMILIANO_ZAPATA.json';
	        document.getElementsByTagName('head')[0].appendChild(emiliano);

	    var filomeno = document.createElement('script');
	        filomeno.src = 'veracruz/FILOMENO_MATA.json';
	        document.getElementsByTagName('head')[0].appendChild(filomeno);

	    var gutierrez = document.createElement('script');
	        gutierrez.src = 'veracruz/GUTIERREZ_ZAMORA.json';
	        document.getElementsByTagName('head')[0].appendChild(gutierrez);

	    var huayaco = document.createElement('script');
	        huayaco.src = 'veracruz/Huayacocotla.json';
	        document.getElementsByTagName('head')[0].appendChild(huayaco);

	    var hueyapan = document.createElement('script');
	        hueyapan.src = 'veracruz/Hueyapan_de_Ocampo.json';
	        document.getElementsByTagName('head')[0].appendChild(hueyapan);

	    var huiloapan = document.createElement('script');
	        huiloapan.src = 'veracruz/Huiloapan_de_Cuauhtemoc.json';
	        document.getElementsByTagName('head')[0].appendChild(huiloapan);

	    var ixhuatlan = document.createElement('script');
	        ixhuatlan.src = 'veracruz/Ixhuatlan_de_Madero.json';
	        document.getElementsByTagName('head')[0].appendChild(ixhuatlan);

	    var cafe = document.createElement('script');
	        cafe.src = 'veracruz/Ixhuatlan_del_Cafe.json';
	        document.getElementsByTagName('head')[0].appendChild(cafe);

	    var ixtaczoq = document.createElement('script');
	        ixtaczoq.src = 'veracruz/Ixtaczoquitlan.json';
	        document.getElementsByTagName('head')[0].appendChild(ixtaczoq);

	    var jesus = document.createElement('script');
	        jesus.src = 'veracruz/Jesus_Carranza.json';
	        document.getElementsByTagName('head')[0].appendChild(jesus);

	    var juan = document.createElement('script');
	        juan.src = 'veracruz/Juan_Rodriguez_Clara.json';
	        document.getElementsByTagName('head')[0].appendChild(juan);

	    var juchique = document.createElement('script');
	        juchique.src = 'veracruz/Juchique_de_Ferrer.json';
	        document.getElementsByTagName('head')[0].appendChild(juchique);

	    var las_Vigas = document.createElement('script');
	        las_Vigas.src = 'veracruz/Las_Vigas_de_Ramirez.json';
	        document.getElementsByTagName('head')[0].appendChild(las_Vigas);

	    var lerdo = document.createElement('script');
	        lerdo.src = 'veracruz/Lerdo_de_Tejada.json';
	        document.getElementsByTagName('head')[0].appendChild(lerdo);

	    var martinez = document.createElement('script');
	        martinez.src = 'veracruz/Martinez_de_la_Torre.json';
	        document.getElementsByTagName('head')[0].appendChild(martinez);

	    var medellin = document.createElement('script');
	        medellin.src = 'veracruz/Medellin_de_Bravo.json';
	        document.getElementsByTagName('head')[0].appendChild(medellin);

	    var minatitlan = document.createElement('script');
	        minatitlan.src = 'veracruz/Minatitlan.json';
	        document.getElementsByTagName('head')[0].appendChild(minatitlan);

	    var mixtla = document.createElement('script');
	        mixtla.src = 'veracruz/Mixtla_de_Altamirano.json';
	        document.getElementsByTagName('head')[0].appendChild(mixtla);

	    var naolinco = document.createElement('script');
	        naolinco.src = 'veracruz/Naolinco.json';
	        document.getElementsByTagName('head')[0].appendChild(naolinco);

	    var naranjal = document.createElement('script');
	        naranjal.src = 'veracruz/Naranjal.json';
	        document.getElementsByTagName('head')[0].appendChild(naranjal);

	    var nautla = document.createElement('script');
	        nautla.src = 'veracruz/Nautla.json';
	        document.getElementsByTagName('head')[0].appendChild(nautla);

	    var oluta = document.createElement('script');
	        oluta.src = 'veracruz/Oluta.json';
	        document.getElementsByTagName('head')[0].appendChild(oluta);

	    var otatitlan = document.createElement('script');
	        otatitlan.src = 'veracruz/Otatitlan.json';
	        document.getElementsByTagName('head')[0].appendChild(otatitlan);

	    var ozuluama = document.createElement('script');
	        ozuluama.src = 'veracruz/Ozuluama_de_Mascarenas.json';
	        document.getElementsByTagName('head')[0].appendChild(ozuluama);

	    var papantla = document.createElement('script');
	        papantla.src = 'veracruz/Papantla.json';
	        document.getElementsByTagName('head')[0].appendChild(papantla);

	    var rafael = document.createElement('script');
	        rafael.src = 'veracruz/Rafael_Delgado.json';
	        document.getElementsByTagName('head')[0].appendChild(rafael);

	    var rio_b = document.createElement('script');
	        rio_b.src = 'veracruz/Rio_Blanco.json';
	        document.getElementsByTagName('head')[0].appendChild(rio_b);

	    var san_adr = document.createElement('script');
	        san_adr.src = 'veracruz/San_Andres_Tuxtla.json';
	        document.getElementsByTagName('head')[0].appendChild(san_adr);

	    var santiag = document.createElement('script');
	        santiag.src = 'veracruz/Santiago_Sochiapan.json';
	        document.getElementsByTagName('head')[0].appendChild(santiag);

	    var sochiapa = document.createElement('script');
	        sochiapa.src = 'veracruz/Sochiapa.json';
	        document.getElementsByTagName('head')[0].appendChild(sochiapa);

	    var soled = document.createElement('script');
	        soled.src = 'veracruz/Soledad_Atzompa.json';
	        document.getElementsByTagName('head')[0].appendChild(soled);

	    var tamalin = document.createElement('script');
	        tamalin.src = 'veracruz/Tamalin.json';
	        document.getElementsByTagName('head')[0].appendChild(tamalin);

	    var tampico = document.createElement('script');
	        tampico.src = 'veracruz/Tampico_Alto.json';
	        document.getElementsByTagName('head')[0].appendChild(tampico);

	    var tantoyuca = document.createElement('script');
	        tantoyuca.src = 'veracruz/Tantoyuca.json';
	        document.getElementsByTagName('head')[0].appendChild(tantoyuca);

	    var tatatila = document.createElement('script');
	        tatatila.src = 'veracruz/Tatatila.json';
	        document.getElementsByTagName('head')[0].appendChild(tatatila);

	    var tehupango = document.createElement('script');
	        tehupango.src = 'veracruz/Tehuipango.json';
	        document.getElementsByTagName('head')[0].appendChild(tehupango);

	    var tenampa = document.createElement('script');
	        tenampa.src = 'veracruz/Tenampa.json';
	        document.getElementsByTagName('head')[0].appendChild(tenampa);

	    var tepa = document.createElement('script');
	        tepa.src = 'veracruz/Tepatlaxco.json';
	        document.getElementsByTagName('head')[0].appendChild(tepa);

	    var tepet = document.createElement('script');
	        tepet.src = 'veracruz/Tepetzintla.json';
	        document.getElementsByTagName('head')[0].appendChild(tepet);

	    var texh = document.createElement('script');
	        texh.src = 'veracruz/Texhuacan.json';
	        document.getElementsByTagName('head')[0].appendChild(texh);

	    var tihua = document.createElement('script');
	        tihua.src = 'veracruz/Tihuatlan.json';
	        document.getElementsByTagName('head')[0].appendChild(tihua);

	    var tlacoj = document.createElement('script');
	        tlacoj.src = 'veracruz/Tlacojalpan.json';
	        document.getElementsByTagName('head')[0].appendChild(tlacoj);

	    var tlacotepec = document.createElement('script');
	        tlacotepec.src = 'veracruz/Tlacotepec_de_Mejia.json';
	        document.getElementsByTagName('head')[0].appendChild(tlacotepec);

	    var tlalnel = document.createElement('script');
	        tlalnel.src = 'veracruz/Tlalnelhuayocan.json';
	        document.getElementsByTagName('head')[0].appendChild(tlalnel);

	    var tlapa = document.createElement('script');
	        tlapa.src = 'veracruz/Tlapacoyancpg.json';
	        document.getElementsByTagName('head')[0].appendChild(tlapa);

	    var tonayan = document.createElement('script');
	        tonayan.src = 'veracruz/Tonayan.json';
	        document.getElementsByTagName('head')[0].appendChild(tonayan);

	    var tres = document.createElement('script');
	        tres.src = 'veracruz/Tres_Valles.json';
	        document.getElementsByTagName('head')[0].appendChild(tres);

	    var tuxt = document.createElement('script');
	        tuxt.src = 'veracruz/Tuxtilla.json';
	        document.getElementsByTagName('head')[0].appendChild(tuxt);

	    var ursulo = document.createElement('script');
	        ursulo.src = 'veracruz/Ursulo_Galvan.json';
	        document.getElementsByTagName('head')[0].appendChild(ursulo);

	   	var villa = document.createElement('script');
	        villa.src = 'veracruz/Villa_Aldama.json';
	        document.getElementsByTagName('head')[0].appendChild(villa);

	    var yanga = document.createElement('script');
	        yanga.src = 'veracruz/Yanga.json';
	        document.getElementsByTagName('head')[0].appendChild(yanga);

	    var zacualp = document.createElement('script');
	        zacualp.src = 'veracruz/Zacualpan.json';
	        document.getElementsByTagName('head')[0].appendChild(zacualp);

	    var zongolica = document.createElement('script');
	        zongolica.src = 'veracruz/Zongolica.json';
	        document.getElementsByTagName('head')[0].appendChild(zongolica);

	    var tierra = document.createElement('script');
	        tierra.src = 'veracruz/TIERRA_BLANCA.json';
	        document.getElementsByTagName('head')[0].appendChild(tierra);
	    
	    /*CAPAS JESUS*/
	    /*var actlan = document.createElement('script');
	        actlan.src = 'veracruz/ACATLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(actlan);

	    var acula = document.createElement('script');
	        acula.src = 'veracruz/ACULA.json';
	        document.getElementsByTagName('head')[0].appendChild(acula);

	    var acult = document.createElement('script');
	        acult.src = 'veracruz/Acultzingo.json';
	        document.getElementsByTagName('head')[0].appendChild(acult);

	    var alpa = document.createElement('script');
	        alpa.src = 'veracruz/Alpatlahuac.json';
	        document.getElementsByTagName('head')[0].appendChild(alpa);  

	    var zonte = document.createElement('script');
	        zonte.src = 'veracruz/Zontecomatlan.json';
	        document.getElementsByTagName('head')[0].appendChild(zonte);

	    var altol = document.createElement('script');
	        altol.src = 'veracruz/ALTOLUCERO.json';
	        document.getElementsByTagName('head')[0].appendChild(altol);

	    var aquila = document.createElement('script');
	        aquila.src = 'veracruz/AQUILA.json';
	        document.getElementsByTagName('head')[0].appendChild(aquila);

	    var camaron = document.createElement('script');
	        camaron.src = 'veracruz/CAMARON_DE_TEJEDA.json';
	        document.getElementsByTagName('head')[0].appendChild(camaron);

	    var atoyac = document.createElement('script');
	        atoyac.src = 'veracruz/ATOYAC.json';
	        document.getElementsByTagName('head')[0].appendChild(atoyac); 

	    var atzalan = document.createElement('script');
	        atzalan.src = 'veracruz/ATZALAN.json';
	        document.getElementsByTagName('head')[0].appendChild(atzalan);

	    var banderilla = document.createElement('script');
	        banderilla.src = 'veracruz/BANDERILLA.json';
	        document.getElementsByTagName('head')[0].appendChild(banderilla);  

	    var calcahualco = document.createElement('script');
	        calcahualco.src = 'veracruz/CALCAHUALCO.json';
	        document.getElementsByTagName('head')[0].appendChild(calcahualco); 

	    var bocar = document.createElement('script');
	        bocar.src = 'veracruz/BOCA_DEL_RIO.json';
	        document.getElementsByTagName('head')[0].appendChild(bocar);

	    var benitoj = document.createElement('script');
	        benitoj.src = 'veracruz/BENITOJUAREZ.json';
	        document.getElementsByTagName('head')[0].appendChild(benitoj);

	    var manlio = document.createElement('script');
	        manlio.src = 'veracruz/Manlio_Fabio_Altamirano.json';
	        document.getElementsByTagName('head')[0].appendChild(manlio);

	    var chiconquiaco = document.createElement('script');
	        chiconquiaco.src = 'veracruz/CHICONQUIACO.json';
	        document.getElementsByTagName('head')[0].appendChild(chiconquiaco);

	    var chicotepec = document.createElement('script');
	        chicotepec.src = 'veracruz/CHICONTEPEC.json';
	        document.getElementsByTagName('head')[0].appendChild(chicotepec);

	    var chinameca = document.createElement('script');
	        chinameca.src = 'veracruz/CHINAMECA.json';
	        document.getElementsByTagName('head')[0].appendChild(chinameca);

	    var chocaman = document.createElement('script');
	        chocaman.src = 'veracruz/CHOCAMAN.json';
	        document.getElementsByTagName('head')[0].appendChild(chocaman);

	    var chontla = document.createElement('script');
	        chontla.src = 'veracruz/CHONTLA.json';
	        document.getElementsByTagName('head')[0].appendChild(chontla);

	    var coatepec = document.createElement('script');
	        coatepec.src = 'veracruz/COATEPEC.json';
	        document.getElementsByTagName('head')[0].appendChild(coatepec);

	    var coatzacoalcos = document.createElement('script');
	        coatzacoalcos.src = 'veracruz/COATZACOALCOS.json';
	        document.getElementsByTagName('head')[0].appendChild(coatzacoalcos);

	    var coacoatzintla = document.createElement('script');
	        coacoatzintla.src = 'veracruz/COACOATZINTLA.json';
	        document.getElementsByTagName('head')[0].appendChild(coacoatzintla);

	    var coatzintla = document.createElement('script');
	        coatzintla.src = 'veracruz/COATZINTLA.json';
	        document.getElementsByTagName('head')[0].appendChild(coatzintla);

	    var colipa = document.createElement('script');
	        colipa.src = 'veracruz/COLIPA.json';
	        document.getElementsByTagName('head')[0].appendChild(colipa);

	    var cordoba = document.createElement('script');
	        cordoba.src = 'veracruz/CORDOBA.json';
	        document.getElementsByTagName('head')[0].appendChild(cordoba);

	    var cosautlan = document.createElement('script');
	        cosautlan.src = 'veracruz/COSAUTLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(cosautlan);

	    var coscomatepec = document.createElement('script');
	        coscomatepec.src = 'veracruz/Coscomatepec.json';
	        document.getElementsByTagName('head')[0].appendChild(coscomatepec);

	    var cotaxtla = document.createElement('script');
	        cotaxtla.src = 'veracruz/COTAXTLA.json';
	        document.getElementsByTagName('head')[0].appendChild(cotaxtla);

	    var coxquihui = document.createElement('script');
	        coxquihui.src = 'veracruz/Coxquihui.json';
	        document.getElementsByTagName('head')[0].appendChild(coxquihui);

	    var cuichapa = document.createElement('script');
	        cuichapa.src = 'veracruz/CUICHAPA.json';
	        document.getElementsByTagName('head')[0].appendChild(cuichapa);

	    var cuitlahuac = document.createElement('script');
	        cuitlahuac.src = 'veracruz/CUITLAHUAC.json';
	        document.getElementsByTagName('head')[0].appendChild(cuitlahuac);

	    var el_higo = document.createElement('script');
	        el_higo.src = 'veracruz/EL_HIGO.json';
	        document.getElementsByTagName('head')[0].appendChild(el_higo);

	    var espinal = document.createElement('script');
	        espinal.src = 'veracruz/ESPINAL.json';
	        document.getElementsByTagName('head')[0].appendChild(espinal);

	    var fortin = document.createElement('script');
	        fortin.src = 'veracruz/FORTIN.json';
	        document.getElementsByTagName('head')[0].appendChild(fortin);

	    var hidalgotitlan = document.createElement('script');
	        hidalgotitlan.src = 'veracruz/HIDALGOTITLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(hidalgotitlan);

	    var huatusco = document.createElement('script');
	        huatusco.src = 'veracruz/HUATUSCO.json';
	        document.getElementsByTagName('head')[0].appendChild(huatusco);

	    var isla = document.createElement('script');
	        isla.src = 'veracruz/ISLA.json';
	        document.getElementsByTagName('head')[0].appendChild(isla);

	    var ilamatlan = document.createElement('script');
	        ilamatlan.src = 'veracruz/ILAMATLAN.json';
	        document.getElementsByTagName('head')[0].appendChild(ilamatlan);

	    var ixcatepec = document.createElement('script');
	        ixcatepec.src = 'veracruz/IXCATEPEC.json';
	        document.getElementsByTagName('head')[0].appendChild(ixcatepec);

	    var ignacio_llave = document.createElement('script');
	        ignacio_llave.src = 'veracruz/Ignacio_de_la_Llave.json';
	        document.getElementsByTagName('head')[0].appendChild(ignacio_llave);

	    var ixhuacan_reyes = document.createElement('script');
	        ixhuacan_reyes.src = 'veracruz/ixhuacan_de_los_Reyes.json';
	        document.getElementsByTagName('head')[0].appendChild(ixhuacan_reyes);

	    var ixhuatlancillo = document.createElement('script');
	        ixhuatlancillo.src = 'veracruz/Ixhuatlancillo.json';
	        document.getElementsByTagName('head')[0].appendChild(ixhuatlancillo);

	    var jalacingo = document.createElement('script');
	        jalacingo.src = 'veracruz/Jalacingo.json';
	        document.getElementsByTagName('head')[0].appendChild(jalacingo);

	    var ixhuatlan_sureste = document.createElement('script');
	        ixhuatlan_sureste.src = 'veracruz/Ixhuatlan_del_Sureste.json';
	        document.getElementsByTagName('head')[0].appendChild(ixhuatlan_sureste);

	    var jalcomulco = document.createElement('script');
	        jalcomulco.src = 'veracruz/Jalcomulco.json';
	        document.getElementsByTagName('head')[0].appendChild(jalcomulco);

	    var ixmatlahuacan = document.createElement('script');
	        ixmatlahuacan.src = 'veracruz/Ixmatlahuacan.json';
	        document.getElementsByTagName('head')[0].appendChild(ixmatlahuacan);

	    var jaltipan = document.createElement('script');
	        jaltipan.src = 'veracruz/Jaltipan.json';
	        document.getElementsByTagName('head')[0].appendChild(jaltipan);

	    var jamapa = document.createElement('script');
	        jamapa.src = 'veracruz/JAMAPA.json';
	        document.getElementsByTagName('head')[0].appendChild(jamapa);

	    var jilotepec = document.createElement('script');
	        jilotepec.src = 'veracruz/JILOTEPEC.json';
	        document.getElementsByTagName('head')[0].appendChild(jilotepec);

	    var la_perla = document.createElement('script');
	        la_perla.src = 'veracruz/La_Perla.json';
	        document.getElementsByTagName('head')[0].appendChild(la_perla);

	    var jose_azueta = document.createElement('script');
	        jose_azueta.src = 'veracruz/Jose_Azueta.json';
	        document.getElementsByTagName('head')[0].appendChild(jose_azueta);

	    var la_antigua = document.createElement('script');
	        la_antigua.src = 'veracruz/La_Antigua.json';
	        document.getElementsByTagName('head')[0].appendChild(la_antigua);

	    var landero_coss = document.createElement('script');
	        landero_coss.src = 'veracruz/Landero_y_Coss.json';
	        document.getElementsByTagName('head')[0].appendChild(landero_coss); 

	    var choapas = document.createElement('script');
	        choapas.src = 'veracruz/Las_Choapas.json';
	        document.getElementsByTagName('head')[0].appendChild(choapas);

	    var minas = document.createElement('script');
	        minas.src = 'veracruz/Las_Minas.json';
	        document.getElementsByTagName('head')[0].appendChild(minas);

	    var los_reyes = document.createElement('script');
	        los_reyes.src = 'veracruz/Los_Reyes.json';
	        document.getElementsByTagName('head')[0].appendChild(los_reyes);

	    var magdalena = document.createElement('script');
	        magdalena.src = 'veracruz/Magdalena.json';
	        document.getElementsByTagName('head')[0].appendChild(magdalena); 

	    var maltrata = document.createElement('script');
	        maltrata.src = 'veracruz/Maltrata.json';
	        document.getElementsByTagName('head')[0].appendChild(maltrata);

	    var mariano_escobedo = document.createElement('script');
	        mariano_escobedo.src = 'veracruz/Mariano_Escobedo.json';
	        document.getElementsByTagName('head')[0].appendChild(mariano_escobedo); 

	    var mecatlan = document.createElement('script');
	        mecatlan.src = 'veracruz/Mecatlan.json';
	        document.getElementsByTagName('head')[0].appendChild(mecatlan);

	    var miahuatlan = document.createElement('script');
	        miahuatlan.src = 'veracruz/Miahuatlan.json';
	        document.getElementsByTagName('head')[0].appendChild(miahuatlan);

	    var misantla = document.createElement('script');
	        misantla.src = 'veracruz/Misantla.json';
	        document.getElementsByTagName('head')[0].appendChild(misantla);

	    var moloacan = document.createElement('script');
	        moloacan.src = 'veracruz/Moloacan.json';
	        document.getElementsByTagName('head')[0].appendChild(moloacan);

	    var nanchital = document.createElement('script');
	        nanchital.src = 'veracruz/Nanchital_de_Lazaro_Cardenas_del_Rio.json';
	        document.getElementsByTagName('head')[0].appendChild(nanchital);

	    var naranjos = document.createElement('script');
	        naranjos.src = 'veracruz/Naranjos_Amatlan.json';
	        document.getElementsByTagName('head')[0].appendChild(naranjos);

	    var nogales = document.createElement('script');
	        nogales.src = 'veracruz/Nogales.json';
	        document.getElementsByTagName('head')[0].appendChild(nogales);

	    var omealca = document.createElement('script');
	        omealca.src = 'veracruz/Omealca.json';
	        document.getElementsByTagName('head')[0].appendChild(omealca);

	    var orizaba = document.createElement('script');
	        orizaba.src = 'veracruz/Orizaba.json';
	        document.getElementsByTagName('head')[0].appendChild(orizaba);

	    var oteapan = document.createElement('script');
	        oteapan.src = 'veracruz/Oteapan.json';
	        document.getElementsByTagName('head')[0].appendChild(oteapan);

	    var pajapan = document.createElement('script');
	        pajapan.src = 'veracruz/Pajapan.json';
	        document.getElementsByTagName('head')[0].appendChild(pajapan);

	    var panuco = document.createElement('script');
	        panuco.src = 'veracruz/Panuco.json';
	        document.getElementsByTagName('head')[0].appendChild(panuco);

	    var paso_ovejas = document.createElement('script');
	        paso_ovejas.src = 'veracruz/Paso_de_Ovejas.json';
	        document.getElementsByTagName('head')[0].appendChild(paso_ovejas);

	    var paso_macho = document.createElement('script');
	        paso_macho.src = 'veracruz/Paso_del_Macho.json';
	        document.getElementsByTagName('head')[0].appendChild(paso_macho);

	    var perote = document.createElement('script');
	        perote.src = 'veracruz/Perote.json';
	        document.getElementsByTagName('head')[0].appendChild(perote);

	    var playa_vicente = document.createElement('script');
	        playa_vicente.src = 'veracruz/Playa_Vicente.json';
	        document.getElementsByTagName('head')[0].appendChild(playa_vicente);

	    var platon_sanchez = document.createElement('script');
	        platon_sanchez.src = 'veracruz/Platon_Sanchez.json';
	        document.getElementsByTagName('head')[0].appendChild(platon_sanchez);

	    var poza_hidalgo = document.createElement('script');
	        poza_hidalgo.src = 'veracruz/Poza_Rica_de_Hidalgo.json';
	        document.getElementsByTagName('head')[0].appendChild(poza_hidalgo);

	    var pueblo_viejo = document.createElement('script');
	        pueblo_viejo.src = 'veracruz/Pueblo_Viejo.json';
	        document.getElementsByTagName('head')[0].appendChild(pueblo_viejo);

	    var puente_nacional = document.createElement('script');
	        puente_nacional.src = 'veracruz/Puente_Nacional.json';
	        document.getElementsByTagName('head')[0].appendChild(puente_nacional);

	    var rafael_lucio = document.createElement('script');
	        rafael_lucio.src = 'veracruz/Rafael_Lucio.json';
	        document.getElementsByTagName('head')[0].appendChild(rafael_lucio);

	    var saltabarranca = document.createElement('script');
	        saltabarranca.src = 'veracruz/Saltabarranca.json';
	        document.getElementsByTagName('head')[0].appendChild(saltabarranca);

	    var san_andres_tenejapan = document.createElement('script');
	        san_andres_tenejapan.src = 'veracruz/San_Andres_Tenejapan.json';
	        document.getElementsByTagName('head')[0].appendChild(san_andres_tenejapan);

	    var san_juan_evangelista = document.createElement('script');
	        san_juan_evangelista.src = 'veracruz/San_Juan_Evangelista.json';
	        document.getElementsByTagName('head')[0].appendChild(san_juan_evangelista);

	    var san_rafael = document.createElement('script');
	        san_rafael.src = 'veracruz/San_Rafael.json';
	        document.getElementsByTagName('head')[0].appendChild(san_rafael);

	    var santiago_tuxtla = document.createElement('script');
	        santiago_tuxtla.src = 'veracruz/Santiago_Tuxtla.json';
	        document.getElementsByTagName('head')[0].appendChild(santiago_tuxtla);

	    var sayula_aleman = document.createElement('script');
	        sayula_aleman.src = 'veracruz/Sayula_de_Aleman.json';
	        document.getElementsByTagName('head')[0].appendChild(sayula_aleman);

	    var soconusco = document.createElement('script');
	        soconusco.src = 'veracruz/Soconusco.json';
	        document.getElementsByTagName('head')[0].appendChild(soconusco);

	    var soledad_doblado = document.createElement('script');
	        soledad_doblado.src = 'veracruz/Soledad_de_Doblado.json';
	        document.getElementsByTagName('head')[0].appendChild(soledad_doblado);

	    var soteapan = document.createElement('script');
	        soteapan.src = 'veracruz/Soteapan.json';
	        document.getElementsByTagName('head')[0].appendChild(soteapan);

	    var tamiahua = document.createElement('script');
	        tamiahua.src = 'veracruz/Tamiahua.json';
	        document.getElementsByTagName('head')[0].appendChild(tamiahua);

	    var tancoco = document.createElement('script');
	        tancoco.src = 'veracruz/Tancoco.json';
	        document.getElementsByTagName('head')[0].appendChild(tancoco);

	    var tantima = document.createElement('script');
	        tantima.src = 'veracruz/Tantima.json';
	        document.getElementsByTagName('head')[0].appendChild(tantima);

	    var tatahuicapan = document.createElement('script');
	        tatahuicapan.src = 'veracruz/Tatahuicapan_de_Juarez.json';
	        document.getElementsByTagName('head')[0].appendChild(tatahuicapan);

	    var tecolutla = document.createElement('script');
	        tecolutla.src = 'veracruz/Tecolutla.json';
	        document.getElementsByTagName('head')[0].appendChild(tecolutla);

	    var tempoal = document.createElement('script');
	        tempoal.src = 'veracruz/Tempoal.json';
	        document.getElementsByTagName('head')[0].appendChild(tempoal);

	    var teocelo = document.createElement('script');
	        teocelo.src = 'veracruz/Teocelo.json';
	        document.getElementsByTagName('head')[0].appendChild(teocelo);

	    var tenochtitlan = document.createElement('script');
	        tenochtitlan.src = 'veracruz/Tenochtitlan.json';
	        document.getElementsByTagName('head')[0].appendChild(tenochtitlan);

	    var tepetlan = document.createElement('script');
	        tepetlan.src = 'veracruz/Tepetlan.json';
	        document.getElementsByTagName('head')[0].appendChild(tepetlan);

	    var tequila = document.createElement('script');
	        tequila.src = 'veracruz/Tequila.json';
	        document.getElementsByTagName('head')[0].appendChild(tequila);

	    var texcatepec = document.createElement('script');
	        texcatepec.src = 'veracruz/Texcatepec.json';
	        document.getElementsByTagName('head')[0].appendChild(texcatepec);

	    var texistepec = document.createElement('script');
	        texistepec.src = 'veracruz/Texistepec.json';
	        document.getElementsByTagName('head')[0].appendChild(texistepec);

	    var tezonapa = document.createElement('script');
	        tezonapa.src = 'veracruz/Tezonapa.json';
	        document.getElementsByTagName('head')[0].appendChild(tezonapa);

	    var tlachichilco = document.createElement('script');
	        tlachichilco.src = 'veracruz/Tlachichilco.json';
	        document.getElementsByTagName('head')[0].appendChild(tlachichilco);

	    var tlacolulan = document.createElement('script');
	        tlacolulan.src = 'veracruz/Tlacolulan.json';
	        document.getElementsByTagName('head')[0].appendChild(tlacolulan);

	    var tlacotalpan = document.createElement('script');
	        tlacotalpan.src = 'veracruz/Tlacotalpan.json';
	        document.getElementsByTagName('head')[0].appendChild(tlacotalpan);

	    var tlalixcoyan = document.createElement('script');
	        tlalixcoyan.src = 'veracruz/Tlalixcoyan.json';
	        document.getElementsByTagName('head')[0].appendChild(tlalixcoyan);

	    var tlaltetela = document.createElement('script');
	        tlaltetela.src = 'veracruz/Tlaltetela.json';
	        document.getElementsByTagName('head')[0].appendChild(tlaltetela);

	    var tlaquilpa = document.createElement('script');
	        tlaquilpa.src = 'veracruz/Tlaquilpa.json';
	        document.getElementsByTagName('head')[0].appendChild(tlaquilpa);

	    var tlilapan = document.createElement('script');
	        tlilapan.src = 'veracruz/Tlilapan.json';
	        document.getElementsByTagName('head')[0].appendChild(tlilapan);

	    var tomatlan = document.createElement('script');
	        tomatlan.src = 'veracruz/Tomatlan.json';
	        document.getElementsByTagName('head')[0].appendChild(tomatlan);

	    var totutla = document.createElement('script');
	        totutla.src = 'veracruz/Totutla.json';
	        document.getElementsByTagName('head')[0].appendChild(totutla);

	    var tuxpan = document.createElement('script');
	        tuxpan.src = 'veracruz/Tuxpan.json';
	        document.getElementsByTagName('head')[0].appendChild(tuxpan);

	    var xico = document.createElement('script');
	        xico.src = 'veracruz/Xico.json';
	        document.getElementsByTagName('head')[0].appendChild(xico);

	    var uxpanapa = document.createElement('script');
	        uxpanapa.src = 'veracruz/Uxpanapa.json';
	        document.getElementsByTagName('head')[0].appendChild(uxpanapa);

	    var vega_torre = document.createElement('script');
	        vega_torre.src = 'veracruz/Vega_de_Alatorre.json';
	        document.getElementsByTagName('head')[0].appendChild(vega_torre);

	    var veracruz = document.createElement('script');
	        veracruz.src = 'veracruz/Veracruz.json';
	        document.getElementsByTagName('head')[0].appendChild(veracruz);

	    var xoxocotla = document.createElement('script');
	        xoxocotla.src = 'veracruz/Xoxocotla.json';
	        document.getElementsByTagName('head')[0].appendChild(xoxocotla);

	    var yecuatla = document.createElement('script');
	        yecuatla.src = 'veracruz/Yecuatla.json';
	        document.getElementsByTagName('head')[0].appendChild(yecuatla);

	    var zaragoza = document.createElement('script');
	        zaragoza.src = 'veracruz/Zaragoza.json';
	        document.getElementsByTagName('head')[0].appendChild(zaragoza);

	    var zentla = document.createElement('script');
	        zentla.src = 'veracruz/Zentla.json';
	        document.getElementsByTagName('head')[0].appendChild(zentla);

	    var zozocolco = document.createElement('script');
	        zozocolco.src = 'veracruz/Zozocolco_de_Hidalgo.json';
	        document.getElementsByTagName('head')[0].appendChild(zozocolco);

	    var mecayapan = document.createElement('script');
	        mecayapan.src = 'veracruz/MECAYAPAN.json';
	        document.getElementsByTagName('head')[0].appendChild(mecayapan);



	           
	}*/