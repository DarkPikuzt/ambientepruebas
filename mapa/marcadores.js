function marker_inspeccion(){
	// Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marcador.length; i++) {
              var position = new google.maps.LatLng(marcador[i][8], marcador[i][7]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                   icon: 'img/icono-inspeccionn.png',
                  map: map,
                  title: marcador[i][1]
              });
               mar.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marcador[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marcador[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Dirección</th>"+"<td>"+marcador[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Descripción</th>"+"<td>"+marcador[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Ingresos</th>"+"<td>"+marcador[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Producto</th>"+"<td>"+marcador[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marcador[i][9]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);


          }
}

// funcion para apagar la capa inspeccion sanitaria
function natAll(map) {
    for (var i = 0; i < mar.length; i++) {
    mar[i].setMap(map);
    }
}
function clearnat() {
        natAll(null);
}

function shownat() {
        natAll(map);
} 

//function principal don de se prende y apaga inspeccion sanitaria

function mostrar_capa_inspeccion_sanitaria(){
  $("#insps").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_inspeccion();
            }else{
                //alert("no");
                clearnat();
            }
    });
}

//Marker para almacenes de granos
function marker_almacen_granos(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marcadoralmg.length; i++) {
              var position = new google.maps.LatLng(marcadoralmg[i][13], marcadoralmg[i][12]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                   icon: 'img/icono-almacen.png',
                  map: map,
                  title: marcadoralmg[i][1]
              });
               almg.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre: <br>" +marcadoralmg[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marcadoralmg[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marcadoralmg[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Domicilio</th>"+"<td>"+marcadoralmg[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Grano principal</th>"+"<td>"+marcadoralmg[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Volumen(ton)</th>"+"<td>"+marcadoralmg[i][7]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Superficie(ha)</th>"+"<td>"+marcadoralmg[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marcadoralmg[i][14]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);


          }
}

// funcion para apagar la capa almacenes de granos
function natAllalmg(map) {
    for (var i = 0; i < almg.length; i++) {
    almg[i].setMap(map);
    }
}
function clearalmg() {
        natAllalmg(null);
}

function shownatalmg() {
        natAllalmg(map);
}

//function principal don de se prende y apaga almacenes de granos

function mostrar_capa_almacen_granos(){
  $("#almg").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_almacen_granos();
            }else{
                //alert("no");
                clearalmg();

            }
    });
}

//marker para la capa sacrificio y procesamiento.
function sacrificio_y_procesamiento(){
    // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_sacrif.length; i++) {
              var position = new google.maps.LatLng(marker_sacrif[i][15], marker_sacrif[i][14]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/icono-centro-sacrificio.png',
                  map: map,
                  title: marker_sacrif[i][7]
              });
               proc.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Empacadora<br>" +marker_sacrif[i][7]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_sacrif[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_sacrif[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Domicilio</th>"+"<td>"+marker_sacrif[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Actividad principal</th>"+"<td>"+marker_sacrif[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Nombre de la especie</th>"+"<td>"+marker_sacrif[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Tipo de mercancia</th>"+"<td>"+marker_sacrif[i][11]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_sacrif[i][16]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}

//funcion para apagar la capa sacrificio y procesamiento.
function sacrifAll(map) {
    for (var i = 0; i < proc.length; i++) {
    proc[i].setMap(map);
    }
}
function clearsacrif() {
        sacrifAll(null);
}

function showsacrif() {
        sacrifAll(map);
}

//function principal don de se prende y apaga inspeccion sanitaria
function mostrar_capa_sacrificio_y_procesamiento(){
  $("#sacrif_proc").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               sacrificio_y_procesamiento();
            }else{
                //alert("no");
                clearsacrif();

            }
    });
}

//marker para la capa beneficios de cafe.
function beneficios_cafe(){
    // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_benef.length; i++) {
              var position = new google.maps.LatLng(marker_benef[i][12], marker_benef[i][11]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/icono-beneficio-cafe.png',
                  map: map,
                  title: marker_benef[i][1]
              });
               benef.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Beneficio<br>" +marker_benef[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Dirección</th>"+"<td>"+marker_benef[i][2]+", "+marker_benef[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_benef[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_benef[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_benef[i][13]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}

//funcion para apagar la capa beneficios de cafe.
function benefAll(map) {
    for (var i = 0; i < benef.length; i++) {
    benef[i].setMap(map);
    }
}
function clearbenef() {
        benefAll(null);
}

function showbenef() {
        benefAll(map);
}

//function principal don de se prende y apaga beneficios de cafe
function mostrar_capa_beneficio_cafe(){
  $("#benef").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               beneficios_cafe();
            }else{
                //alert("no");
                clearbenef();

            }
    });
}

//marker para la capa de centros de acopio SELGAMEX.
function centros_acopio(){
    // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_centroacopio.length; i++) {
              var position = new google.maps.LatLng(marker_centroacopio[i][9], marker_centroacopio[i][8]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/icono-centro-acopio.png',
                  map: map,
                  title: marker_centroacopio[i][1]
              });
               centroac.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>SELGAMEX<br>" +marker_centroacopio[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Dirección</th>"+"<td>"+marker_centroacopio[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_centroacopio[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_centroacopio[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_centroacopio[i][10]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}

//funcion para apagar la capa centroaicios de cafe.
function centroaAll(map) {
    for (var i = 0; i < centroac.length; i++) {
    centroac[i].setMap(map);
    }
}
function clearcentroa() {
        centroaAll(null);
}

function showcentroa() {
        centroaAll(map);
}

//function principal don de se prende y apaga beneficios de cafe
function mostrar_capa_centro_acopio(){
  $("#centroa").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               centros_acopio();
            }else{
                //alert("no");
                clearcentroa();

            }
    });
}

/*Marcador para la capa de ingenieros_azucareros*/
function marker_ingenieros(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_azucareros.length; i++) {
              var position = new google.maps.LatLng(marker_azucareros[i][8], marker_azucareros[i][7]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/icono-cana-azucar.png',
                  map: map,
                  title: marker_azucareros[i][3]
              });
               azucarera.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_azucareros[i][3]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_azucareros[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Región</th>"+"<td>"+marker_azucareros[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Corporativo</th>"+"<td>"+marker_azucareros[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_azucareros[i][9]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);


          }
}

//funcion para apagar la capa ingenieros azucareros.
function azucarfAll(map) {
    for (var i = 0; i < azucarera.length; i++) {
    azucarera[i].setMap(map);
    }
}
function clearazucar() {
        azucarfAll(null);
}

function showazucar() {
        azucarfAll(map);
}

//function principal donde se apaga y prende la capa ingenios azucareros
function mostrar_capa_ingenieros_azucareros(){
  $("#ing_azu").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_ingenieros();
            }else{
                //alert("no");
                clearazucar();

            }
    });

}

//////////////////////////////////////////////////////////////////////////
/*Capa de unidades de pesca*/
function marker_unidad_pesca(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_pesc.length; i++) {
              var position = new google.maps.LatLng(marker_pesc[i][9], marker_pesc[i][8]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/icono-uds-produccionn.png',
                  map: map,
                  title: marker_pesc[i][1]
              });
               pesca.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_pesc[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_pesc[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_pesc[i][7]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Actividad</th>"+"<td>"+marker_pesc[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_pesc[i][10]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);


          }
}

//funcion para apagar la capa unidad de pesca.
function pescaAll(map) {
    for (var i = 0; i < pesca.length; i++) {
    pesca[i].setMap(map);
    }
}
function clearpesca() {
        pescaAll(null);
}

function showpesca() {
        pescaAll(map);
}

/*function que permite apagar y prender capa unidad de pesca*/

function mostrar_unidad_pesca(){
  $("#unit_pesca").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
              marker_unidad_pesca();
            }else{
                //alert("no");
              clearpesca();

            }
    });

}    

/**CAPA PARA RASTROS TIF MUNICIPAL*/
function markers_rastros_tif_municipal(){
    // Colocamos los marcadores en el Mapa de Google 
    for (i = 0; i < marker_centros_municipal.length; i++) {
      var position = new google.maps.LatLng(marker_centros_municipal[i][7], marker_centros_municipal[i][6]);
      //bounds.extend(position);
      marker = new google.maps.Marker({
          position: position,
          icon: 'img/icono-centro-sacrificio-municipal.png',
          map: map,
          title: marker_centros_municipal[i][1]
      });
      tif_municipal.push(marker);
      (function(i, marker) {
      google.maps.event.addListener(marker,'click',function() {
      if (!infowindow) {
      infowindow = new google.maps.InfoWindow({maxWidth: 300});
      }
      infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_centros_municipal[i][4]+
        "</div>"+
        "<table id='style_table' class='table table-sm table-bordered'>"+
        "<thead>"+
        "</thead>"+
        "<tbody>"+
        "<tr>"+
        "<th>Municipio</th>"+"<td>"+marker_centros_municipal[i][1]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Localidad</th>"+"<td>"+marker_centros_municipal[i][2]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Domicilio</th>"+"<td>"+marker_centros_municipal[i][3]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Tipo</th>"+"<td>"+marker_centros_municipal[i][5]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Capacidad mensual para sacrificio bovinos (número de cabezas)</th>"+"<td>"+marker_centros_municipal[i][8]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Capacidad mensual para sacrificio porcinos (número de cabezas)</th>"+"<td>"+marker_centros_municipal[i][9]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Capacidad mensual para sacrificio caprinos (número de cabezas) </th>"+"<td>"+marker_centros_municipal[i][10]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Capacidad mensual para sacrificio ovinos (número de cabezas)</th>"+"<td>"+marker_centros_municipal[i][11]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Capacidad mensual para sacrificio avícola (número de cabezas)</th>"+"<td>"+marker_centros_municipal[i][12]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Capacidad mensual para sacrificio equinos (número de cabezas)</th>"+"<td>"+marker_centros_municipal[i][13]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Estatus</th>"+"<td>"+marker_centros_municipal[i][14]+"</td>"+
        "</tr>"+
        "<tr>"+
        "<th>Fuente</th>"+"<td>"+marker_centros_municipal[i][15]+"</td>"+
        "</tr>"+
        "</tbody>"+
        "</table>");
      infowindow.open(map, marker);
        map.setZoom(10);
        map.setCenter(marker.getPosition());               
      });
      })(i, marker);
   }
}

//funcion para apagar la capa unidad de pesca.
function tifmunicipalAll(map) {
  for (var i = 0; i < tif_municipal.length; i++) {
  tif_municipal[i].setMap(map);
  }
}
function cleartifmunicipal() {
      tifmunicipalAll(null);
}

function showtifmunicipal() {
      tifmunicipalAll(map);
}

/*function que permite apagar y prender capa de rastros tif municipal*/
function mostrar_rastros_tif_municipal(){
  $("#tifmunicipal").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               markers_rastros_tif_municipal();
            }else{
                //alert("no");
                cleartifmunicipal();
            }
    });

}

///////////////////////////////////////////////////////
/*Capa de establos lecheros*/
function marker_establos_lecheros(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_establos.length; i++) {
              var position = new google.maps.LatLng(marker_establos[i][5], marker_establos[i][4]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/establos_lecheros.png',
                  map: map,
                  title: marker_establos[i][1]
              });
               establos.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_establos[i][6]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_establos[i][1]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_establos[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Domicilio</th>"+"<td>"+marker_establos[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Raza principal</th>"+"<td>"+marker_establos[i][9]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Tipo de ordeña</th>"+"<td>"+marker_establos[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Superficie de la instalación (m2)</th>"+"<td>"+marker_establos[i][11]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Estatus</th>"+"<td>"+marker_establos[i][12]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>fuente</th>"+"<td>"+marker_establos[i][13]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);


          }
}


//funcion para apagar la capa unidad de pesca.
function establoslAll(map) {
  for (var i = 0; i < establos.length; i++) {
  establos[i].setMap(map);
  }
}
function clearestablos() {
      establoslAll(null);
}

function showestablos() {
      establoslAll(map);
}

/*function que permite apagar y prender capa de rastros tif municipal*/
function mostrar_establos_lecheros(){
  $("#establos").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
              marker_establos_lecheros();
            }else{
                //("no");
              clearestablos();
            }
    });

}

//////////////////////////////////////////////////////////////////////////
/*Capa de corrales de engorda*/
function marker_corrales_engorda(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_corrales.length; i++) {
              var position = new google.maps.LatLng(marker_corrales[i][12], marker_corrales[i][11]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  icon: 'img/corral_engorda.png',
                  map: map,
                  title: marker_corrales[i][1]
              });
              corrales.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_corrales[i][4]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_corrales[i][1]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_corrales[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Domicilio</th>"+"<td>"+marker_corrales[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Sistema productivo intensivo</th>"+"<td>"+marker_corrales[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Sistema productivo semi-intensivo</th>"+"<td>"+marker_corrales[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Capacidad instalada (número de cabezas</th>"+"<td>"+marker_corrales[i][7]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Raza en producción</th>"+"<td>"+marker_corrales[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Periodo de engorda (meses)</th>"+"<td>"+marker_corrales[i][9]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Superficie de la instalación (m2)</th>"+"<td>"+marker_corrales[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Estatus</th>"+"<td>"+marker_corrales[i][13]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_corrales[i][14]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);


          }
}


//funcion para apagar la capa de corrales de engorda.
function corraleslAll(map) {
  for (var i = 0; i < corrales.length; i++) {
  corrales[i].setMap(map);
  }
}
function clearcorrales() {
      corraleslAll(null);
}

function showcorrales() {
      corraleslAll(map);
}

/*function que permite apagar y prender capa de corrales de engorda*/
function mostrar_corrales_de_engorda(){
  $("#corrales").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_corrales_engorda();
            }else{
                //alert("no");
                clearcorrales();
            }
    });

}
/**capa de granjas de ave 2019*/
function marker_granjas_aves(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_granj.length; i++) {
              var position = new google.maps.LatLng(marker_granj[i][13], marker_granj[i][12]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                   icon: 'img/granja_ave.png',
                  map: map,
                  title: marker_granj[i][1]
              });
              ave.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_granj[i][4]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_granj[i][1]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_granj[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Domicilio</th>"+"<td>"+marker_granj[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Sistema productivo tecnificado</th>"+"<td>"+marker_granj[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Capacidad instalada (número de cabezas)</th>"+"<td>"+marker_granj[i][7]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Número de naves</th>"+"<td>"+marker_granj[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Superficie de la instalación (m2)</th>"+"<td>"+marker_granj[i][9]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Estatus</th>"+"<td>"+marker_granj[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_granj[i][11]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}

//funcion para apagar la capa de granjas de ave.
function granjaslAll(map) {
  for (var i = 0; i < ave.length; i++) {
  ave[i].setMap(map);
  }
}
function cleargranjas() {
      granjaslAll(null);
}

function showgranjas() {
      granjaslAll(map);
}
/*function que permite apagar y prender capa de granjas de ave 2019*/

function mostrar_granjas_de_ave_2019(){
  $("#granjas_ave").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_granjas_aves();
            }else{
                //alert("no");
                cleargranjas();
            }
    });

}
///////////////////////////////////////
//capa de presas
function marker_capa_presas(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_presa.length; i++) {
              var position = new google.maps.LatLng(marker_presa[i][10], marker_presa[i][9]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                   icon: 'img/presa.png',
                  map: map,
                  title: marker_presa[i][1]
              });
              presas.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>Presa<br>" +marker_presa[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Nombre común</th>"+"<td>"+marker_presa[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Región</th>"+"<td>"+marker_presa[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Ubicación</th>"+"<td>"+marker_presa[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Altura de la cortina (m)</th>"+"<td>"+marker_presa[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Capacidad NAME (hm3)</th>"+"<td>"+marker_presa[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Capacidad NAMO (hm3)</th>"+"<td>"+marker_presa[i][7]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Volumen almacenado (hm3)</th>"+"<td>"+marker_presa[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_presa[i][11]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
              map.setZoom(10);
              map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}
//funcion para apagar la capa de presas.
function presasAll(map) {
  for (var i = 0; i < presas.length; i++) {
  presas[i].setMap(map);
  }
}
function clearpresas() {
      presasAll(null);
}

function showpresas() {
      presasAll(map);
}
/*function que permite apagar y prender capa de presas*/
function mostrar_capa_presas(){
  $("#capa_presas").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_capa_presas();
            }else{
                //alert("no");
                clearpresas();
            }
    });

}
/////////////
//capa de oficinas cader
function marker_capa_oficader(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_oficader.length; i++) {
              var position = new google.maps.LatLng(marker_oficader[i][14], marker_oficader[i][13]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                   icon: 'img/ofi_cader.png',
                  map: map,
                  title: marker_oficader[i][1]
              });
              oficader.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>CADER<br>" +marker_oficader[i][2]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_oficader[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_oficader[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Dirección</th>"+"<td>"+marker_oficader[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Jefe</th>"+"<td>"+marker_oficader[i][6]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Teléfono</th>"+"<td>"+marker_oficader[i][7]+" Extensión "+marker_oficader[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Email</th>"+"<td>"+marker_oficader[i][9]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Hora de inicio</th>"+"<td>"+marker_oficader[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Hora de fin</th>"+"<td>"+marker_oficader[i][11]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Días laborales</th>"+"<td>"+marker_oficader[i][12]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_oficader[i][15]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
              map.setZoom(10);
              map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}
//funcion para apagar la capa de presas.
function oficaderAll(map) {
  for (var i = 0; i < oficader.length; i++) {
  oficader[i].setMap(map);
  }
}
function clearoficader() {
      oficaderAll(null);
}

function showoficader() {
      oficaderAll(map);
}
/*function que permite apagar y prender capa de presas*/
function mostrar_capa_oficader(){
  $("#ofi_cader").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_capa_oficader();
            }else{
                //alert("no");
                clearoficader();
            }
    });

}
//////////////////
//capa de oficinas ddr
function marker_capa_ofiddr(){
  // Colocamos los marcadores en el Mapa de Google 
          for (i = 0; i < marker_ofiddr.length; i++) {
              var position = new google.maps.LatLng(marker_ofiddr[i][13], marker_ofiddr[i][12]);
              //bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                   icon: 'img/ofi_ddr.png',
                  map: map,
                  title: marker_ofiddr[i][1]
              });
              ofiddr.push(marker);
              (function(i, marker) {
              google.maps.event.addListener(marker,'click',function() {
              if (!infowindow) {
              infowindow = new google.maps.InfoWindow({maxWidth: 300});
              }
              infowindow.setContent("<div id='style_markers'>DDR<br>" +marker_ofiddr[i][1]+
                "</div>"+
                "<table id='style_table' class='table table-sm table-bordered'>"+
                "<thead>"+
                "</thead>"+
                "<tbody>"+
                "<tr>"+
                "<th>Municipio</th>"+"<td>"+marker_ofiddr[i][2]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Localidad</th>"+"<td>"+marker_ofiddr[i][3]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Dirección</th>"+"<td>"+marker_ofiddr[i][4]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Jefe</th>"+"<td>"+marker_ofiddr[i][5]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Teléfono</th>"+"<td>"+marker_ofiddr[i][6]+" Extensión "+marker_ofiddr[i][7]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Email</th>"+"<td>"+marker_ofiddr[i][8]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Hora de inicio</th>"+"<td>"+marker_ofiddr[i][9]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Hora de fin</th>"+"<td>"+marker_ofiddr[i][10]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Días laborales</th>"+"<td>"+marker_ofiddr[i][11]+"</td>"+
                "</tr>"+
                "<tr>"+
                "<th>Fuente</th>"+"<td>"+marker_ofiddr[i][14]+"</td>"+
                "</tr>"+
                "</tbody>"+
                "</table>");
              infowindow.open(map, marker);
              map.setZoom(10);
              map.setCenter(marker.getPosition());               
              });
              })(i, marker);
          }
}
//funcion para apagar la capa de presas.
function ofiddrAll(map) {
  for (var i = 0; i < ofiddr.length; i++) {
  ofiddr[i].setMap(map);
  }
}
function clearofiddr() {
      ofiddrAll(null);
}

function showofiddr() {
      ofiddrAll(map);
}
/*function que permite apagar y prender capa de presas*/
function mostrar_capa_ofiddr(){
  $("#ofi_ddr").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_capa_ofiddr();
            }else{
               //alert("no");
                clearofiddr();
            }
    });

}
//////////////////////////////////
/**Capa de granjas de huevo 2019*/
function marker_granjas_huevo(){
      // Colocamos los marcadores en el Mapa de Google 
      for (i = 0; i < marker_huevo.length; i++) {
        var position = new google.maps.LatLng(marker_huevo[i][13], marker_huevo[i][12]);
        //bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            icon: 'img/granja_huevo.png',
            map: map,
            title: marker_huevo[i][1]
        });
        huevo.push(marker);
        (function(i, marker) {
        google.maps.event.addListener(marker,'click',function() {
        if (!infowindow) {
        infowindow = new google.maps.InfoWindow({maxWidth: 300});
        }
        infowindow.setContent("<div id='style_markers'>Nombre:<br>" +marker_huevo[i][4]+
          "</div>"+
          "<table id='style_table' class='table table-sm table-bordered'>"+
          "<thead>"+
          "</thead>"+
          "<tbody>"+
          "<tr>"+
          "<th>Municipio</th>"+"<td>"+marker_huevo[i][1]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Localidad</th>"+"<td>"+marker_huevo[i][2]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Domicilio</th>"+"<td>"+marker_huevo[i][3]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Sistema productivo tecnificado</th>"+"<td>"+marker_huevo[i][5]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Capacidad instalada (número de cabezas)</th>"+"<td>"+marker_huevo[i][7]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Número de aves</th>"+"<td>"+marker_huevo[i][8]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Superficie de la instalación (m2)</th>"+"<td>"+marker_huevo[i][9]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Estatus</th>"+"<td>"+marker_huevo[i][10]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Fuente</th>"+"<td>"+marker_huevo[i][11]+"</td>"+
          "</tr>"+
          "</tbody>"+
          "</table>");
        infowindow.open(map, marker);
          map.setZoom(10);
          map.setCenter(marker.getPosition());               
        });
        })(i, marker);
      }

}

//funcion para apagar la capa de granjas de huevo.
function huevolAll(map) {
  for (var i = 0; i < huevo.length; i++) {
  huevo[i].setMap(map);
  }
}
function clearhuevo() {
      huevolAll(null);
}

function showhuevo() {
      huevolAll(map);
}

/*function que permite apagar y prender capa de granjas de HUEVO 2019*/
function mostrar_granjas_de_huevo_2019(){
  $("#granjas_huevo").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_granjas_huevo();
            }else{
                //alert("no");
                clearhuevo();
            }
    });

}

/**Capa de granjas de huevo 2019*/
function marker_pozos_perote(){
      // Colocamos los marcadores en el Mapa de Google 
      for (i = 0; i < marker_pozos.length; i++) {
        var position = new google.maps.LatLng(marker_pozos[i][4], marker_pozos[i][3]);
        //bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            icon: 'img/pozos_perote.png',
            map: map,
            title: marker_pozos[i][1]
        });
        pozos.push(marker);
        (function(i, marker) {
        google.maps.event.addListener(marker,'click',function() {
        if (!infowindow) {
        infowindow = new google.maps.InfoWindow({maxWidth: 300});
        }
        infowindow.setContent("<div id='style_markers'>" +marker_pozos[i][1]+
          "</div>"+
          "<table id='style_table' class='table table-sm table-bordered'>"+
          "<thead>"+
          "</thead>"+
          "<tbody>"+
          "<tr>"+
          "<th>Volumen total de extracción (m3/año)</th>"+"<td>"+marker_pozos[i][2]+"</td>"+
          "</tr>"+
          "<tr>"+
          "<th>Fuente</th>"+"<td>"+marker_pozos[i][5]+"</td>"+
          "</tr>"+
          "</tbody>"+
          "</table>");
        infowindow.open(map, marker);
          map.setZoom(10);
          map.setCenter(marker.getPosition());               
        });
        })(i, marker);
      }

}

//funcion para apagar la capa de granjas de pozos.
function pozoslAll(map) {
  for (var i = 0; i < pozos.length; i++) {
  pozos[i].setMap(map);
  }
}
function clearpozos() {
      pozoslAll(null);
}

function showpozos() {
      pozoslAll(map);
}

/*function que permite apagar y prender capa pozos perote*/
function mostrar_puntos_pozos(){
  $("#pozos_perote").on('change',function(){
            if ($(this).is(':checked') ) {
               //alert("si");
               marker_pozos_perote();
            }else{
                //alert("no");
                clearpozos();
            }
    });

}

//////////////////////////////////
/**CAPA DE PUERTOS DE VERACRUZ*/
function marker_puertos_veracruz(){
  // Colocamos los marcadores en el Mapa de Google 
  for (i = 0; i < marker_puertos.length; i++) {
    var position = new google.maps.LatLng(marker_puertos[i][4], marker_puertos[i][3]);
    //bounds.extend(position);
    marker = new google.maps.Marker({
        position: position,
        icon: 'img/puertos.png',
        map: map,
        title: marker_puertos[i][1]
    });
    puerto.push(marker);
    (function(i, marker) {
    google.maps.event.addListener(marker,'click',function() {
    if (!infowindow) {
    infowindow = new google.maps.InfoWindow({maxWidth: 300});
    }
    infowindow.setContent("<div id='style_markers'>Municipio:<br>" +marker_puertos[i][1]+
      "</div>"+
      "<table id='style_table' class='table table-sm table-bordered'>"+
      "<thead>"+
      "</thead>"+
      "<tbody>"+
      "<tr>"+
      "<th>Descripción</th>"+"<td>"+marker_puertos[i][2]+"</td>"+
      "</tr>"+
      "<tr>"+
      "<th>Fuente</th>"+"<td>SIAP</td>"+
      "</tr>"+
      "</tbody>"+
      "</table>");
    infowindow.open(map, marker);
      map.setZoom(10);
      map.setCenter(marker.getPosition());               
    });
    })(i, marker);
  }

}

//funcion para apagar la capa de puertos de veracruz.
function puertosAll(map) {
  for (var i = 0; i < puerto.length; i++) {
  puerto[i].setMap(map);
  }
}
function clearpuerto() {
      puertosAll(null);
}

function showpuerto() {
      puertosAll(map);
}

/*function que permite apagar y prender capa de puertos de veracruz*/
function mostrar_puertos_veracruz(){
  $("#puertos").on('change',function(){
            if ($(this).is(':checked') ) {
              marker_puertos_veracruz();
            }else{
              clearpuerto();
            }
    });

}
